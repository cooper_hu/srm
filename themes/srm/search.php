<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package SRM
 */

get_header(); ?>
	<main id="primary" class="site-main">

		<section class="section xs-margin">
			<div class="container">
				<div class="">
					<?php get_template_part( 'template-parts/search-bar'); ?>
				</div>
			</div>
		</section>


		<?php 
		$search_results_per_page = get_option( 'posts_per_page' );

		$args = array(
			'post_type' => array( 'perspective', 'article', 'video', 'podcast', 'infographic' ),
			'posts_per_page' => $search_results_per_page,
			'suppress_filters' => false,
			's' => get_search_query(),
			'relevanssi'  => true,
		);

		$the_query = new WP_Query( $args ); ?>

		<?php if ( $the_query->have_posts() ) : ?>
			<?php $total_posts = $the_query->found_posts; ?>
			<section class="section">
				<div class="container">
					<div class="container__inner-full">

						<header class="page-header">
							<h1 class="h2">
								<?php printf(esc_html__( 'Searched for: ', 'srm')); ?>
								<?= get_search_query(); ?>
							</h1>
						</header><!-- .page-header -->

						<div class="category-subnav xs-margin" data-search-term="<?= get_search_query(); ?>">
							
							<a class="js-filter-search-results active" href="#all" data-post-type="all" data-post-count="<?= $total_posts; ?>"><span>All (<?= $total_posts; ?>)</span></a>

							<?php // Get the current category object
							
							// Get all custom post types
							$args = array(
								'public'   => true, // Get only public post types
								'_builtin' => false, // Exclude WordPress default post types like 'post' and 'page'
							);

							$post_types = get_post_types($args, 'objects');

							// Display the post count for each post type in this category
							foreach ($post_types as $post_type) {
								// Arguments to count posts in this category and of this custom post type
								$query_args = array(
									'post_type' => $post_type->name,
									'posts_per_page' => -1, // No limit
									'suppress_filters' => false,
									's' => get_search_query(),
									'relevanssi'  => true,
								);
								
								// Execute the query
								$query = new WP_Query($query_args);

								// Get the post count
								$post_count = $query->found_posts;

								if ($post_count > 0) {
									echo '<a class="js-filter-search-results" href="#' . strtolower($post_type->name) . '" data-post-type="' . strtolower($post_type->name) . '" data-post-count="' . $post_count . '"><span>' . $post_type->labels->name . ' (' . $post_count . ')</span></a>';
								}
								
								// Reset postdata after each query
								wp_reset_postdata();
							} ?>
							
							<div class="sort-filter-wrapper">
								<span><?php esc_html_e( 'Sort By', 'srm' ); ?>:</span>
								<select class="sort-filter js-sort-results" data-paged="1" data-post-type="all" data-init-post-count="<?= $posts_per_page; ?>" data-total-posts="<?= $total_posts; ?>">
									<option value="relevance"><?php esc_html_e( 'Relevance', 'srm' ); ?></option>
									<option value="newest"><?php esc_html_e( 'Newest - Oldest', 'srm' ); ?></option>
									<option value="oldest"><?php esc_html_e( 'Oldest - Newest', 'srm' ); ?></option>
									<option value="ASC"><?php esc_html_e( 'A - Z', 'srm' ); ?></option>
									<option value="DESC"><?php esc_html_e( 'Z - A', 'srm' ); ?></option>
								</select>
							</div>
						</div>

						<div class="archive-grid">
							<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<?php get_template_part( 'template-parts/archive-card'); ?>
							<?php endwhile; ?>
						</div>

						<?php
						
						if ($search_results_per_page < $total_posts) : ?>
							<div class="archive-grid-footer">
								<button class="btn blue-gradient wide js-load-more-search" 
									data-paged="1" 
									data-post-type="all"
									data-init-post-count="<?= $posts_per_page; ?>"
									data-total-posts="<?= $total_posts; ?>">
									Load More
								</button>
								<div class="loading-spinner" style="display: none;">Loading...</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</section>
			
			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<section class="section padded-lg">
				<div class="container">
					<div class="container__inner">
						<h1>
							<?php esc_html_e( 'Nothing Found', 'srm' ); ?>
						</h1>

						<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with a different keyword.', 'srm' ); ?></p>
						
						<div class="search-page-form">
							<?php get_search_form(); ?>
						</div>
	
					</div>
				</div>
			</section>

		<?php endif; ?>

		<?php
		
		/*
		if ( have_posts() ) : ?>
			<section class="section xs-margin">
				<div class="container">
					<div class="container__inner-full">
						
						<header class="page-header">
							<h1 class="h2">
								<?php printf(esc_html__( 'Searched for: ', 'srm')); ?>
								<?= get_search_query(); ?>
							</h1>
						</header><!-- .page-header -->

						<div class="archive-subnav">
							<a href="">Articles (21)</a>
						</div>

						<div class="archive-grid">
							<?php while ( have_posts() ) : the_post();
								get_template_part( 'template-parts/archive-card');
							endwhile; ?>
						</div>

						<?php the_posts_navigation(); ?>
					</div>
				</div>
			</section>
		<?php else : ?>
			<section class="section ">
				<div class="container">
					<div class="container__inner">
						<h1>
							<?php esc_html_e( 'Nothing Found', 'srm' ); ?>
						</h1>

						<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with a different keyword.', 'srm' ); ?></p>
						
						<div class="search-page-form">
							<?php get_search_form(); ?>
						</div>
	
					</div>
				</div>
			</section>
		<?php endif; 
		*/ ?>

	</main>
<?php get_footer(); ?>
