<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SRM
 */

get_header();
?>

	<main id="primary" class="site-main">
		<section class="section xs-margin">
			<div class="container">
				<div class="">
					<?php get_template_part( 'template-parts/search-bar'); ?>
				</div>
			</div>
		</section>

		<section class="section">
			<div class="container">
				<div class="container__inner">
					<?php if ( have_posts() ) : ?>
                        <h1 class="h2">
                        <?php printf(esc_html__('Upcoming Events', 'srm')); ?>
                        </h1>
                        <div class="accordion-group">
                            <?php while ( have_posts() ) : the_post(); ?>
                                <div class="accordion-item">
                                    <div class="accordion-item__header event">
                                        <h4><?= get_field('event_date'); ?></h4>
                                        <h2><?php the_title(); ?></h2>
                                        <h5><?= get_field('event_location'); ?></h5>
                                    </div>
                                    <div class="accordion-item__content">
                                        <div class="accordion-item__content--inner">
                                            <?= get_field('event_description'); ?>
                                        </div>
                                    </div>
                                </div>
							<?php endwhile; ?>
                        </div>
					<?php endif; ?>
				</div>
			</div>
		</section>

	</main><!-- #main -->

<?php get_footer();
