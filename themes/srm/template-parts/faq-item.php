<div class="accordion-item js-fade-in">
                                <div class="accordion-item__header">
                                    <h4><?php the_title(); ?></h4>
                                </div>
                                <div class="accordion-item__content">
                                    <div class="accordion-item__content--inner small">
                                        <?= get_field('answer'); ?>
                                    </div>
                                </div>
                            </div>