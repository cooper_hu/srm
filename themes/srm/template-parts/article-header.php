<?php // fields
if (get_post_type(get_the_ID()) === 'post') {
	$post_name = 'Article';
} else {
	$post_name = ucfirst(get_post_type(get_the_ID()));
}

$authors = get_field('authors');
$reviewers = get_field('reviewers');
$featured_people = get_field('featured_people');

$subhead = get_field('subhead');
$lead = get_field('lead'); ?>

<header class="article-header">
    <div class="container">
	    <div class="container__inner">
            <?php if (get_field('include_header_image')) :
                $header_image = get_field('header_image'); ?>
                <div  class="article-header__image">
                    <img src="<?= $header_image['url']; ?>" alt="<?= $header_image['alt']; ?>"/>
                    <?php if ($header_image['caption']) : ?>
                        <p class="article-header__image--caption"><?= $header_image['caption']; ?></p>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php
            $post_type = get_post_type($post->ID);
            $post_type_object = get_post_type_object($post_type);
            $original_label = $post_type_object->labels->singular_name;

            $translated_label = apply_filters('wpml_translate_single_string', $original_label, 'Custom Post Type', "Post Type Label: $post_type - singular_name"); ?>
        
            <h3 class="article-type">
                <a href="<?= get_post_type_archive_link(get_post_type(get_the_ID())); ?>">
                <?= $translated_label; ?>
                </a>
            </h3>
            
            <h1 class="article-headline"><?php the_title(); ?></h1>
            
            <?php if ($subhead) : ?>
                <?= str_replace( array('<p', '/p>'), array('<h2 class="article-subhead"', '/h2>'), $subhead ); ?>
            <?php endif; ?>

            <?php if (get_post_type(get_the_ID()) !== 'infographic') :
                include get_template_directory() . '/template-parts/article-meta.php';
            endif; ?>

        </div>
    </div>
</header>