<div class="search-bar">
    <div class="search-bar__tags">
        <div class="search-bar__tags--inner">
            <?php /*
            <a class="btn teal" href="/video/what-is-srm/">What Is SRM?</a>
            <a class="btn teal" href="/category/stratospheric-aerosol-injection/">Stratospheric Aerosol Injection</a>
            <a class="btn teal" href="/podcast/history-of-srm/">History of SRM</a>
*/ ?>

            <?php if (have_rows('search_bar_buttons', 'option')) : ?>
                <?php printf(esc_html__( 'Featured: ', 'srm')); ?>
                <?php while (have_rows('search_bar_buttons', 'option')) : the_row(); ?>
                    <?php $link = get_sub_field('button'); // Get the link field
                    if ($link) : 
                        $url = $link['url'];
                        $title = $link['title'];
                        $target = $link['target'] ? $link['target'] : '_self'; ?>
                        
                        <a class="btn teal" href="<?= esc_url($url); ?>" target="<?= esc_attr($target); ?>"><?= esc_html($title); ?></a>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="search-bar__form">
        <?= get_search_form(); ?>
    </div>
</div>