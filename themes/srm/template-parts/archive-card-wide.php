<?php
if (is_int($post)) {
    $post = get_post($post); // Fetch the post object if $post is an ID (integer)
} else if (is_string($post)) {
    $post = get_post(intval($post));
}

if ($post && !is_wp_error($post)) {
    $post_type = $post->post_type;
} else {
    // Handle error or missing post
    $post_type = '';
} ?>

<div class="archive-card-wide">
    <div class="archive-card-wide__left">
        <?php if ($post_type === 'infographic') : ?>
            <div class="archive-card-wide__infographic">
                <div class="archive-card-wide__infographic--body">
                    <a href="<?php the_permalink(); ?>">
                        <?= get_the_post_thumbnail(); ?>
                    </a>
                </div>

                <?php // Source
                $include_source = get_field('include_source');
                $info_source = get_field('infographic_source'); ?>

                <?php if ($include_source) : ?>
                    <div class="archive-card-wide__infographic--footer">
                        <?php // printf(esc_html__( 'Source: ', 'srm')); ?>
                        <?php if ($info_source['url']) : ?>
                            <a href="<?= $info_source['url']; ?>" target="_blank"><?= $info_source['name']; ?></a>
                        <?php else : ?>
                            <span><?= $info_source['name']; ?></span>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php elseif ($post_type === 'video') : ?>
            <?php $cover_image = get_field('cover_image'); 
            $video = get_field('video');
        
            // Use preg_match to find iframe src.
            preg_match('/src="(.+?)"/', $video, $matches);
            if ($matches) : 
                $video_url = $matches[1]; ?>
                <div class="archive-card-wide__image">
                    <a href="<?= $video_url; ?>" class="article-video-player js-play-video">
                        <img src="<?= $cover_image['url']; ?>" alt="<?= $cover_image['alt']; ?>"/>
                    </a>            
                </div>
            <?php endif; ?>
        <?php else : ?>
            <div class="archive-card-wide__image">
                <a href="<?php the_permalink(); ?>">
                    <?= get_the_post_thumbnail(); ?>
                </a>
            </div>
        <?php endif; ?>
    </div>
    <div class="archive-card-wide__right">
        <div>
            <h2 class="archive-card-wide-title">
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>    
            </h2>
            <?php $allowedTags = "<sub><sup>"; ?>
            <h3 class="archive-card-wide-subhead">
                <?= strip_tags(get_field('subhead'), $allowedTags); ?>
            </h3>

            <?php
            // Authors 
            $authors = get_field('authors');

            if ($authors) : 
                $length = count($authors);
                $i = 0; ?>
                
                <h4 class="archive-card-wide-author">  
                    <?php printf(esc_html__( 'By ', 'srm')); ?>
                    <?php foreach( $authors as $author ) : ?> 
                        <a href="<?= get_the_permalink($author); ?>"><?= get_the_title($author); ?></a><?php echo (++$i < $length ? ', ' : ''); ?>
                    <?php endforeach; ?>
                </h5>
               
            <?php endif; ?>
            
            <p><?= get_field('lead'); ?> <a class="bold" href="<?php the_permalink(); ?>">Read More</a></p>

        </div>
    </div>
</div>