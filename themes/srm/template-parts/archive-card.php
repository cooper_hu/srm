
<?php
if (is_int($post)) {
    $post = get_post($post); // Fetch the post object if $post is an ID (integer)
} else if (is_string($post)) {
    $post = get_post(intval($post));
}

if ($post && !is_wp_error($post)) {
    $post_type = $post->post_type;
} else {
    // Handle error or missing post
    $post_type = '';
}

$allowedTags = "<sub><sup>"; ?>

<a href="<?php the_permalink(); ?>" class="archive-card">
    <div class="archive-card__inner">
        
        <?php if (!isset($hide_label)) : ?>
            <div class="archive-card__meta">
                <div>
                    <?php if (is_int($post)) {
                        $post = get_post($post); // Fetch the post object if $post is an ID (integer)
                    }

                    if ($post && !is_wp_error($post)) {
                        $post_type = $post->post_type;
                    } else {
                        // Handle error or missing post
                        $post_type = '';
                    } ?>

                    
                    <?php
                    $post_type = get_post_type($post->ID);
                    $post_type_object = get_post_type_object($post_type);
                    $original_label = $post_type_object->labels->singular_name;

                    // Get the translated label using WPML
                    $translated_label = apply_filters('wpml_translate_single_string', $original_label, 'Custom Post Type', "Post Type Label: $post_type - singular_name"); ?>
                    
                    <h5><?= $translated_label; ?></h5>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($post->post_type === 'video') : 
            $cover_image = get_field('cover_image');  ?>
            <div class="archive-card__img video">    
                <img src="<?= $cover_image['url']; ?>" alt="<?= $cover_image['alt']; ?>"/>
            </div>
        <?php else : ?>
            <?php if (get_the_post_thumbnail_url(get_the_ID())) : ?>
                <div class="archive-card__img">        
                    <img src="<?= get_the_post_thumbnail_url(); ?>"/>
                </div>
            <?php endif; ?>
        <?php endif; ?>

        <h3 class="archive-card-title"><?php the_title(); ?></h3>

        <?php if (get_the_post_thumbnail_url()) : ?>
            <?php $subhead_length = strlen(get_field('subhead')); ?>
            
            <?php if ($subhead_length > 200) : ?>
                <p><?= substr(strip_tags(get_field('subhead'), $allowedTags), 0, 125); ?>...</p>
            <?php else : ?>
                <p><?= strip_tags(get_field('subhead'), $allowedTags); ?></p>
            <?php endif; ?>
        <?php else : ?>
            <?php if (isset($subhead_length)) : ?>
                <?php if ($subhead_length > 300) : ?>
                    <p><?= substr(strip_tags(get_field('subhead'), $allowedTags), 0, 250); ?>...</p>
                <?php else : ?>
                    <p><?= strip_tags(get_field('subhead'), $allowedTags); ?></p>
                <?php endif; ?>
            <?php else : ?>
                <p><?= strip_tags(get_field('subhead'), $allowedTags); ?></p>
            <?php endif; ?>
        <?php endif; ?>
        

        <?php if (get_field('read_time')) : ?>
            <p class="bold">
                <?= get_field('read_time'); ?> 

                <?php if ($post->post_type === 'article' || $post->post_type === 'perspective') : ?>
                    <?php printf(esc_html__('min read', 'srm')); ?>
                <?php else : ?>
                    <?php printf(esc_html__('mins', 'srm')); ?>
                <?php endif; ?>
            </p>
        <?php endif; ?>
    </div>
</a>