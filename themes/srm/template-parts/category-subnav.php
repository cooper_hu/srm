<div class="category-subnav">
    <?php // Get the current category object
    $category = get_queried_object();
    
    // Get all custom post types
    $args = array(
        'public'   => true, // Get only public post types
        '_builtin' => false, // Exclude WordPress default post types like 'post' and 'page'
    );

    $post_types = get_post_types($args, 'objects');

    // Display the post count for each post type in this category
    foreach ($post_types as $post_type) {
        // Arguments to count posts in this category and of this custom post type
        $query_args = array(
            'post_type' => $post_type->name,
            'posts_per_page' => -1, // No limit
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'term_id',
                    'terms'    => $category->term_id,
                ),
            ),
            'fields' => 'ids', // We only need post IDs to count
        );
        
        // Execute the query
        $query = new WP_Query($query_args);

        
        // Get the post count
        $post_count = $query->found_posts;

        if ($post_count > 0) {
            echo '<a class="js-scroll-to-section" href="#' . strtolower($post_type->name) . '"><span>' . $post_type->labels->name . ' (' . $post_count . ')</span></a>';
        }
        
        // Reset postdata after each query
        wp_reset_postdata();
    } ?>
</div>