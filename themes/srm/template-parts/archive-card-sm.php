
<?php $allowedTags = "<sub><sup>"; ?>
<a href="<?php the_permalink(); ?>" class="archive-card sm">
    <div class="archive-card__inner">

        <h3 class="archive-card-title"><?php the_title(); ?></h3>

        <p><?= strip_tags(substr(get_field('subhead'), 0, 125), $allowedTags); ?>...</p>

        <?php if (get_field('read_time')) : ?>
            <p class="bold">
                <?= get_field('read_time'); ?> 

                <?php if ($post->post_type === 'article' || $post->post_type === 'perspective') : ?>
                    <?php printf(esc_html__('min read', 'srm')); ?>
                <?php else : ?>
                    <?php printf(esc_html__('mins', 'srm')); ?>
                <?php endif; ?>
            </p>
        <?php endif; ?>
    </div>
</a>