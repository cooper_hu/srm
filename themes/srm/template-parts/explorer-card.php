<a href="<?php the_permalink(); ?>" class="explorer-card">
    <?php if ($post->post_type === 'video') : 
        $cover_image = get_field('cover_image');  ?>
        <div class="explorer-card__img">
            <div class="explorer-card__img--img video">
                <img src="<?= $cover_image['url']; ?>" alt="<?= $cover_image['alt']; ?>"/>
            </div>
        </div>
    <?php else : ?>
        <?php if (get_the_post_thumbnail_url()) : ?>
            <div class="explorer-card__img">
                <div class="explorer-card__img--img">
                    <img src="<?= get_the_post_thumbnail_url(); ?>"/>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <div class="explorer-card__content">
        <div class="explorer-card__content--meta">
            <?php
            $post_type = get_post_type($post->ID);
            $post_type_object = get_post_type_object($post_type);
            $original_label = $post_type_object->labels->singular_name;

            $translated_label = apply_filters('wpml_translate_single_string', $original_label, 'Custom Post Type', "Post Type Label: $post_type - singular_name"); ?>
            
            <h5 class="css-capitalize"><?= $translated_label; ?></h5>

            <?php if (get_field('read_time')) : ?>
                <h5>
                    <?= get_field('read_time'); ?> 
                    <?php if ($post->post_type === 'article' || $post->post_type === 'perspective') : ?>
                        <?php printf(esc_html__('min read', 'srm')); ?>
                    <?php else : ?>
                        <?php printf(esc_html__('mins', 'srm')); ?>
                    <?php endif; ?>
                </h5>
            <?php endif; ?>
        </div>

        <h3><?php the_title(); ?></h3>
        <?php $allowedTags = "<sub><sup>"; ?>
        <p><?= strip_tags(get_field('subhead'), $allowedTags); ?></p>
    </div>
</a>