<div class="article-meta">
    <?php if (get_post_type(get_the_ID()) === 'perspective') :
        $perspective_people = get_field('people'); ?>
            <?php if ($perspective_people) : ?>
            <div class="article-meta__featured">
            
                <div class="article-meta__misc--item">
                    
                </div>
                <div class="article-featured-grid">
                    <?php foreach( $perspective_people as $featured_person ) : ?> 
                        <a class="article-featured-grid__item" href="<?= get_the_permalink($featured_person); ?>">
                            <div class="article-featured-grid__item--img">
                                <?= get_the_post_thumbnail($featured_person, 'thumbnail'); ?>
                            </div>
                            <?= get_the_title($featured_person); ?>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php // Video 
    if (get_post_type(get_the_ID()) != 'video') : ?>
        <?php
        // Authors 
        if ($authors) : 
            $length = count($authors);
            $i = 0; ?>
            <div class="article-meta__byline">
                <span class="bold">
                    <?php if (get_post_type(get_the_ID()) === 'podcast') : ?>
                        <?php printf(esc_html__( 'Hosted By: ', 'srm')); ?>
                    <?php else : ?>
                        <?php printf(esc_html__( 'Authored By: ', 'srm')); ?>
                    <?php endif; ?>
                </span> 
                <?php foreach( $authors as $author ) : ?> 
                    <a href="<?= get_the_permalink($author); ?>"><?= get_the_title($author); ?></a><?php echo (++$i < $length ? ', ' : ''); ?>
                    <?php // Format author for citation
                    // $citation_name = get_field('last_name', $author) .', ' . get_field('first_name', $author) . '; '; 
                    $citation_name = get_the_title($author) . ', ';
                    $citation_list .= $citation_name; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <div class="article-meta__misc">
        <?php // Reviewers 
        if ($reviewers) : 
            $length = count($reviewers);
            $i = 0; ?>
            <div class="article-meta__misc--item">
                <span class="bold">
                    <?php printf(esc_html__( 'Reviewed By: ', 'srm')); ?>
                </span> 
                <?php foreach( $reviewers as $reviewer ) : ?> 
                    <a href="<?= get_the_permalink($reviewer); ?>"><?= get_the_title($reviewer); ?></a><?php echo (++$i < $length ? ', ' : ''); ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <?php // Featuring  ?>
        <?php if ($featured_people) : 
            $length = count($featured_people);
            $i = 0; ?>
            <div class="article-meta__misc--item">
                <span class="bold">
                    <?php printf(esc_html__( 'Featuring: ', 'srm')); ?>
                </span> 
                <?php foreach( $featured_people as $featured_person ) : ?> 
                    <a href="<?= get_the_permalink($featured_person); ?>"><?= get_the_title($featured_person); ?></a><?php echo (++$i < $length ? ', ' : ''); ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        

        <?php // Publish Date ?>
        <div class="article-meta__misc--item">
            <div class="article-meta-date">
                <span class="bold">
                    <?php printf(esc_html__( 'Published: ', 'srm')); ?>
                </span> 
                <?= get_the_date(); ?>
            </div>

            <?php if (get_field('last_revised_date')) : ?>
                <div class="article-revised-date">
                    <span class="bold">
                        <?php printf(esc_html__( 'Last revised: ', 'srm')); ?>
                    </span> 
                    <?= get_field('last_revised_date'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="article-meta__citation">
        <div>
            <a class="js-scroll-to-section" href="#citation">
                <?php printf(esc_html__( 'Cite This ', 'srm')); ?>
                <?php printf(esc_html__( $post_name, 'srm')); ?>
            </a>
        </div>

        <?php if (get_post_type(get_the_ID()) === 'infographic') : 
            $download_link = get_field('downloadable_graphic'); 
            if ($download_link) : ?>
            <div class="article-meta__citation--download">
                <a href="<?= $download_link['url']; ?>" download>
                    <?php printf(esc_html__( 'Download Graphic', 'srm')); ?>
                </a>
            </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>

<div class="article-share">
    <?php // Facebook ?>
    <button id="facebook-share-button" class="share-button fb">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M80 299.3V512H196V299.3h86.5l18-97.8H196V166.9c0-51.7 20.3-71.5 72.7-71.5c16.3 0 29.4 .4 37 1.2V7.9C291.4 4 256.4 0 236.2 0C129.3 0 80 50.5 80 159.4v42.1H14v97.8H80z"/></svg>
    </button>
    <?php // Twitter ?>
    <button id="twitter-share-button" class="share-button x">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M389.2 48h70.6L305.6 224.2 487 464H345L233.7 318.6 106.5 464H35.8L200.7 275.5 26.8 48H172.4L272.9 180.9 389.2 48zM364.4 421.8h39.1L151.1 88h-42L364.4 421.8z"/></svg>
    </button>
    <?php // Linkedin ?>
    <button id="linkedin-share-button" class="share-button linkedin">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M100.3 448H7.4V148.9h92.9zM53.8 108.1C24.1 108.1 0 83.5 0 53.8a53.8 53.8 0 0 1 107.6 0c0 29.7-24.1 54.3-53.8 54.3zM447.9 448h-92.7V302.4c0-34.7-.7-79.2-48.3-79.2-48.3 0-55.7 37.7-55.7 76.7V448h-92.8V148.9h89.1v40.8h1.3c12.4-23.5 42.7-48.3 87.9-48.3 94 0 111.3 61.9 111.3 142.3V448z"/></svg>
    </button>
    <?php // Bluesky ?>
    <button id="bluesky-share-button" class="share-button bluesky">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M111.8 62.2C170.2 105.9 233 194.7 256 242.4c23-47.6 85.8-136.4 144.2-180.2c42.1-31.6 110.3-56 110.3 21.8c0 15.5-8.9 130.5-14.1 149.2C478.2 298 412 314.6 353.1 304.5c102.9 17.5 129.1 75.5 72.5 133.5c-107.4 110.2-154.3-27.6-166.3-62.9l0 0c-1.7-4.9-2.6-7.8-3.3-7.8s-1.6 3-3.3 7.8l0 0c-12 35.3-59 173.1-166.3 62.9c-56.5-58-30.4-116 72.5-133.5C100 314.6 33.8 298 15.7 233.1C10.4 214.4 1.5 99.4 1.5 83.9c0-77.8 68.2-53.4 110.3-21.8z"/></svg>
    </button>
    <?php // Whatsapp ?>
    <button id="whatsapp-share-button" class="share-button whatsapp">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7 .9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z"/></svg>
    </button>
    <?php // Email ?>
    <button id="email-share-button" class="share-button email">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M48 64C21.5 64 0 85.5 0 112c0 15.1 7.1 29.3 19.2 38.4L236.8 313.6c11.4 8.5 27 8.5 38.4 0L492.8 150.4c12.1-9.1 19.2-23.3 19.2-38.4c0-26.5-21.5-48-48-48L48 64zM0 176L0 384c0 35.3 28.7 64 64 64l384 0c35.3 0 64-28.7 64-64l0-208L294.4 339.2c-22.8 17.1-54 17.1-76.8 0L0 176z"/></svg>
    </button>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        const postUrl = "<?php echo get_permalink(); ?>"; // Dynamically get the current post URL
        const postTitle = "<?php echo html_entity_decode(get_the_title()); ?>"; // Get the post title
        
        const fbShareUrl = `https://www.facebook.com/sharer/sharer.php?u=${encodeURIComponent(postUrl)}&quote=${encodeURIComponent(postTitle)}`;
        const twitterShareUrl = `https://twitter.com/intent/tweet?url=${encodeURIComponent(postUrl)}&text=${encodeURIComponent(postTitle)}`;
        const linkedinShareUrl = `https://www.linkedin.com/sharing/share-offsite/?url=${encodeURIComponent(postUrl)}`;
        const emailShareUrl = `mailto:?subject=${encodeURIComponent(postTitle)}&body=${encodeURIComponent(postTitle + '\n' + postUrl)}`;
        const whatsappShareUrl = `https://wa.me/?text=${encodeURIComponent(postTitle + '\n' + postUrl)}`;
        const blueskyShareUrl = `https://bsky.app/share?text=${encodeURIComponent(postTitle + '\n' + postUrl)}`;

        document.getElementById('facebook-share-button').addEventListener('click', function () {
            window.open(fbShareUrl, "_blank", "width=600,height=400");
        });

        document.getElementById('twitter-share-button').addEventListener('click', function () {
            window.open(twitterShareUrl, "_blank", "width=600,height=400");
        });

        document.getElementById('linkedin-share-button').addEventListener('click', function () {
            window.open(linkedinShareUrl, "_blank", "width=600,height=400");
        });

        document.getElementById('email-share-button').addEventListener('click', function () {
            window.location.href = emailShareUrl;
        });

        document.getElementById('whatsapp-share-button').addEventListener('click', function () {
            window.open(whatsappShareUrl, "_blank");
        });

        document.getElementById('bluesky-share-button').addEventListener('click', function () {
            window.open(blueskyShareUrl, "_blank", "width=600,height=400");
        });
    });
</script>