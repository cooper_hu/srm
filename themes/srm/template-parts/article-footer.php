
<?php if (get_field('include_transcript')) : ?>
    <section class="section">
        <div class="container">
            <div class="container__inner">
                <div class="accordion-group">
                    
                    <div class="accordion-item rounded">
                        <div class="accordion-item__header">
                            <h3 class="css-no-margin"><?php printf(esc_html__( 'Transcript', 'srm')); ?></h2>
                        </div>
                        <div class="accordion-item__content">
                            <div class="accordion-item__content--inner small">
                                <?= get_field('transcript'); ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if (get_post_type(get_the_ID()) !== 'infographic') : 
    // Hide form on infographic page ?>
    <section class="section">
        <div class="container">
            <div class="container__inner">
                <div class="article-form">
                    <div class="article-form__icon"></div>
                    <div class="article-form__form-wrapper">
                        <h4 class="h2"><?php printf(esc_html__( 'Ask us a question!', 'srm')); ?></h4>
                        <?php echo FrmFormsController::get_form_shortcode( array( 'id' => 2 ) ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php $related_content = get_field('related_articles');
 if ($related_content) : ?>
    <section class="section" aria-hidden="true" role="presentation">
        <div class="container">
            <div class="container__inner-full">
                <div class="archive-grid">
                    <?php foreach( $related_content as $post ) : setup_postdata($post); ?>
                        <?php get_template_part( 'template-parts/archive-card'); ?>
                    <?php endforeach; ?>
                </div>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </section>
<?php else : ?>
    <?php // Related Content
    $categories = get_the_category(get_the_ID());

    // Check if the post has categories assigned
    if ($categories && !is_wp_error($categories)) : 
        // Use the first category as the primary category
        $primary_category = $categories[0]->term_id;

        $args = array(
            'post_type' => array( 'perspective', 'article', 'video', 'podcast', 'infographic' ),
            'posts_per_page' => 3,
            'post__not_in' => array(get_the_ID()),
            'suppress_filters' => false,
            'cat'            => $primary_category, // Filter by primary category
            'orderby'        => 'rand', 
        );

        $the_query = new WP_Query( $args ); ?>

        <?php if ( $the_query->have_posts() ) : ?>
            <section class="section" aria-hidden="true" role="presentation">
                <div class="container">
                    <div class="container__inner-full">
                        <div class="article-related">
                            <h2><?php printf(esc_html__( 'Related Content', 'srm')); ?></h2>
                            <div class="archive-grid">
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <?php get_template_part( 'template-parts/archive-card'); ?>
                                <?php endwhile; ?>
                            </div>

                            <?php wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>

<?php if (get_field('include_endnotes')): ?>
    <section class="section blue padded no-margin" id="endnotes">
        <div class="container">
            <div class="container__inner">
                <div class="article-endnotes">
                    <h3><?php printf(esc_html__( 'Endnotes', 'srm')); ?></h3>
                    <div class="article-endnotes__inner">
                        <?= get_field('endnotes'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<section class="section teal padded no-margin" id="citation">
    <div class="container">
        <div class="container__inner">
            <div class="article-citation-header">
                <h3 class="xs-bottom-margin"><?php printf(esc_html__( 'Citation', 'srm')); ?></h3>
                <p class="no-top-margin" aria-hidden="true" role="presentation" tabindex="-1">
                    <?php printf(esc_html__('
                    Our content relies on a range of sources. When citing this '.$post->post_type.', please also cite the underlying sources where applicable. This '.$post->post_type.' can be cited as follows:', 'srm')); ?>
                </p>
            </div>
            <div class="css-relative">
                <?php /*
                <div class="article-citation" id="articleCitation">
                    <?= substr($citation_list, 0, -2); ?> (<?= get_the_date('Y'); ?>) - "<?php the_title(); ?>" <?php printf(esc_html__( 'Published online at SRM360.org', 'srm')); ?>. <?php printf(esc_html__( 'Retrieved from: ', 'srm')); ?>'<?php the_permalink(); ?>' [<?php printf(esc_html__( 'Online Resource', 'srm')); ?>]
                </div>
                */ ?>


                <div class="article-citation" id="articleCitation">
                    <?php if (strlen($citation_list) > 1) : ?>
                        <?= substr($citation_list, 0, -2); ?>
                    <?php else : ?>
                        SRM360 
                    <?php endif; ?>
                    (<?= get_the_date('Y'); ?>) – "<?php the_title(); ?>" [<?= ucfirst($post->post_type); ?>]. <?php printf(esc_html__( 'Published online at SRM360.org', 'srm')); ?>. <?php printf(esc_html__( 'Retrieved from: ', 'srm')); ?>'<?php the_permalink(); ?>' [<?php printf(esc_html__( 'Online Resource', 'srm')); ?>]
                    <?php 
                    $last_revised = get_field('last_revised_date');
                    if ($last_revised) : ?>
                        <?php printf(esc_html__( 'Last revised', 'srm')); ?>: <?= $last_revised; ?>
                    <?php endif; ?>
                </div>

                <button class="copy-citation-btn" id="js-copy-citation">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--!Font Awesome Free 6.6.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M208 0L332.1 0c12.7 0 24.9 5.1 33.9 14.1l67.9 67.9c9 9 14.1 21.2 14.1 33.9L448 336c0 26.5-21.5 48-48 48l-192 0c-26.5 0-48-21.5-48-48l0-288c0-26.5 21.5-48 48-48zM48 128l80 0 0 64-64 0 0 256 192 0 0-32 64 0 0 48c0 26.5-21.5 48-48 48L48 512c-26.5 0-48-21.5-48-48L0 176c0-26.5 21.5-48 48-48z"/></svg>
                    <span class="copy-citation-tooltip js-citation-tooltip"><?php printf(esc_html__( 'Copied!', 'srm')); ?></span>
                </button>
            </div>
        </div>
    </div>
</section>

<section class="section grey no-margin padded">
    <div class="container">
        <div class="container__inner">
            <h3 class="xs-bottom-margin"><?php printf(esc_html__( 'Reuse this work freely', 'srm')); ?></h3>
            <p class="bold no-top-margin"><?php printf(esc_html__( 'The content produced by SRM360 is open access under the Creative Commons BY license. You are free to use, distribute, and reproduce these in any medium, provided that SRM360 and the authors are credited.', 'srm')); ?></p>
            <p class="bold"><?php printf(esc_html__( 'The sources used by SRM360 are subject to the licence terms of the original third party. We will always indicate the original sources in our content, so please review the licence of any third-party sources before use and redistribution. ', 'srm')); ?></p>
        </div>
    </div>
</section>

<script>
    let text = '';
    document.addEventListener('DOMContentLoaded', function() {
        text = document.getElementById('articleCitation').innerHTML.trim();
        console.log('text', text);
    });

    document.getElementById('js-copy-citation').addEventListener('click', function() {
        copyToClipboard(text);
    });

    function copyToClipboard(text) {
        if (navigator.clipboard) {
            navigator.clipboard.writeText(text).then(function() {
                // console.log('Text copied to clipboard successfully!');
                flashTooltip()
            }).catch(function(err) {
                console.error('Could not copy text: ', err);
            });
        } else {
            // Fallback for older browsers
            let textArea = document.createElement('textarea');
            textArea.value = text;
            document.body.appendChild(textArea);
            textArea.select();
            try {
                document.execCommand('copy');
                // console.log('Text copied to clipboard using fallback method!');
                flashTooltip();
            } catch (err) {
                console.error('Fallback: Oops, unable to copy', err);
            }
            // document.body.removeChild(textArea);
        }
        
    }

    function flashTooltip() {
        const tooltip = document.querySelector('.js-citation-tooltip');
        tooltip.classList.add('active');
        // Remove the class 'active' after 3 seconds (3000 milliseconds)
        setTimeout(() => {
            tooltip.classList.remove('active');
        }, 2000);
    }
</script>

