<?php  // Infographic

$infographic_size = get_field('infographic_size');

if (!isset($infographic_size)) {
    $infographic_size = 'container__inner-full'; // if not set
}

if (get_post_type(get_the_ID()) === 'infographic') :
    $infographic_type = get_field('infographic_type');  ?>
    <section class="article-media">
        <div class="container">
            <div class="<?= $infographic_size; ?>">
                <?php if ($infographic_type === 'static') : 
                    $infographic_image = get_field('infographic_image');
                    $infographic_image_mobile = get_field('infographic_image_mobile'); ?>
                    <div class="article-infographic">
                        <?php if ($infographic_image_mobile) : ?>
                            <img class="article-infographic__mobile" src="<?= $infographic_image_mobile['url']; ?>" alt="<?= $infographic_image_mobile['alt']; ?>"/>
                            <img class="article-infographic__desktop" src="<?= $infographic_image['url']; ?>" alt="<?= $infographic_image['alt']; ?>"/>
                        <?php else : ?>
                            <img src="<?= $infographic_image['url']; ?>" alt="<?= $infographic_image['alt']; ?>"/>
                        <?php endif; ?>
                    </div>
                <?php elseif ($infographic_type === 'ai2html') :
                    $ai2html_url = get_field('ai2html_url');
                    $aiurl =  parse_url($ai2html_url, PHP_URL_PATH); ?>
                    <div class="article-infographic">
					    <?php include ltrim($aiurl, '/');  ?>
                    </div>
                <?php endif; ?>

                <?php if (get_field('include_source')) : 
                    $infographic_source = get_field('infographic_source'); ?>
                    <div class="article-infographic__source">
                        <?php // printf(esc_html__( 'Source: ', 'srm')); ?>
                        <?php if ($infographic_source['url']) : ?>
                            <a href="<?= $infographic_source['url']; ?>" target="_blank"><?= $infographic_source['name']; ?></a>
                        <?php else : ?>
                            <span><?= $infographic_source['name']; ?></span>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </section>
<?php elseif (get_post_type(get_the_ID()) === 'video') :
    $cover_image = get_field('cover_image'); 
    $video = get_field('video');
   
    // Use preg_match to find iframe src.
    preg_match('/src="(.+?)"/', $video, $matches);
    if ($matches) : 
        $video_url = $matches[1]; ?>
        <section class="article-media">
            <div class="container">
                <div class="container__inner">
                    <a href="<?= $video_url; ?>" class="article-video-player js-play-video">
                        <img src="<?= $cover_image['url']; ?>" alt="<?= $cover_image['alt']; ?>"/>
                    </a>            
                </div>
            </div>
        </section>
    <?php endif; ?>
<?php elseif (get_post_type(get_the_ID()) === 'podcast') : ?>
    <section class="article-media">
        <div class="container">
            <div class="container__inner">
                <?php if (has_post_thumbnail()) : ?>
                    <div class="article-podcast-image">
                        <?php the_post_thumbnail('full'); ?>
                    </div> 
                <?php endif; ?>
                
                <div id="single-song-player" class="podcast-player" data-mp3="<?= get_field('mp3_url'); ?>">
                    <div class="amplitude-play-pause" id="play-pause"></div>
                    <div class="podcast-player__meta">
                        <progress class="amplitude-song-played-progress" id="song-played-progress"></progress>
                        
                        
                        <div class="time-container">
                            <span class="current-time">
                                <span class="amplitude-current-minutes"></span>:<span class="amplitude-current-seconds"></span>
                            </span>
                            <span class="duration">
                                <span class="amplitude-duration-minutes"></span>:<span class="amplitude-duration-seconds"></span>
                            </span>
                        </div>
                        <?php /* */ ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if (get_post_type(get_the_ID()) === 'infographic') : ?>
    <div class="container">
        <div class="container__inner">
            <?php include get_template_directory() . '/template-parts/article-meta.php'; ?>
        </div>
    </div>
<?php endif; ?>