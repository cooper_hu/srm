<section class="section">
    <div class="container">
        <div class="container__inner">
            <div class="summary-card">
                <h2><?= get_sub_field('summary_headline'); ?></h2>

                <?php if( have_rows('summary_point') ): ?>
                    <ul>
                        <?php while ( have_rows('summary_point') ) : the_row(); ?>
                            <li><?= get_sub_field('item'); ?></li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
                
            </div>
        </div>
    </div>
</section>