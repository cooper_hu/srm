<section class="section small-margin">
    <div class="container">
        <div class="container__inner">
            <div class="text-wrapper">
                <?= get_sub_field('textblock'); ?>
            </div>
        </div>
    </div>
</section>