<section class="section">
    <div class="container">
        <div class="container__inner">
            <h2><?= get_sub_field('team_headline'); ?></h2>
            <div>
                <?= get_sub_field('team_description'); ?>
            </div>

            <?php // Team
            $team_members = get_sub_field('people'); ?>
            
            <?php if ( $team_members ) : ?>
                <div class="team-grid">
                    <?php foreach( $team_members as $post ): setup_postdata($post); ?>
                        <a class="team-item" href="<?= get_the_permalink($post->id); ?>">
                            <div class="team-item__img">
                                <?= get_the_post_thumbnail($post->id, 'thumbnail'); ?>
                            </div>
                            <?= get_the_title($post->ID); ?>
                            <?php if (get_sub_field('show_titles')) : ?>
                                <br/>
                                <span class="job-title"><?= get_field('job_title', $post->ID); ?></span>
                            <?php endif; ?>
                        </a>
                    <?php endforeach; ?>
                </div>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>

        </div>
    </div>
</section>

