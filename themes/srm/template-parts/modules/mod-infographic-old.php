<?php // fields
$infographic_title = get_sub_field('infographic_title');
$infographic_caption = get_sub_field('infographic_caption');
$infographic_source = get_sub_field('infographic_source');
$infographic_type = get_sub_field('type');
$infographic_size = get_sub_field('size'); ?>

<section class="section">
    <div class="container">
        <div class="<?php  if ($infographic_size === 'small' || $infographic_size === 'small-side-by-side' ) { echo 'container__inner'; } else { echo 'container__inner-wide'; } ?>">
            <?php if ($infographic_size === 'large' || $infographic_size === 'small' ) : ?>
                <div class="srm-table">
                    <?php // GOES HERE
                    if ($infographic_type === 'infographic-custom') :
                    $info_image = get_sub_field('custom_infographic');
                    $info_image_mobile = get_sub_field('custom_infographic_mobile'); ?>
                        <div class="srm-table__image">
                            <?php if ($info_image_mobile) : ?>
                                <img class="srm-table__image--mobile" src="<?= $info_image_mobile['url']; ?>" alt="<?= $info_image_mobile['alt']; ?>"/>
                                <img class="srm-table__image--desktop" src="<?= $info_image['url']; ?>" alt="<?= $info_image['alt']; ?>"/>
                            <?php else : ?>
                                <img src="<?= $info_image['url']; ?>" alt="<?= $info_image['alt']; ?>"/>
                            <?php endif; ?>
                        </div>
                    <?php elseif ($infographic_type === 'infographic-cpt') :
                        $info_cpt_id = get_sub_field('infographic');
                        $info_image = get_field('infographic_image', $info_cpt_id);
                        $info_image_mobile = get_field('custom_infographic_mobile', $info_cpt_id) ?>
                        
                        <div class="srm-table__image">
                            <?php if ($info_image_mobile) : ?>
                                <img class="srm-table__image--mobile" src="<?= $info_image_mobile['url']; ?>" alt="<?= $info_image_mobile['alt']; ?>" loading="lazy"/>
                                <img class="srm-table__image--desktop" src="<?= $info_image['url']; ?>" alt="<?= $info_image['alt']; ?>" loading="lazy"/>
                            <?php else : ?>
                                <img src="<?= $info_image['url']; ?>" alt="<?= $info_image['alt']; ?>" loading="lazy"/>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($infographic_caption) : ?>
                        <div class="srm-table__caption">
                            <h3><?= $infographic_title; ?></h3>
                            <p><?= $infographic_caption; ?></p>
                        </div>
                    <?php endif; ?>

                    <?php // GOES HERE
                    if ($infographic_type === 'infographic-custom') : ?>
                        <?php if (get_sub_field('include_source')) : ?>
                            <div class="srm-table__source">
                                <?php printf(esc_html__( 'Source: ', 'srm')); ?>
                                <a href="<?= $infographic_source['url']; ?>" target="_blank"><?= $infographic_source['name']; ?></a>
                            </div>
                        <?php endif; ?>
                    <?php elseif ($infographic_type === 'infographic-cpt') :
                        $info_cpt_id = get_sub_field('infographic'); ?>
                        <?php if (get_field('include_source', $info_cpt_id)) : 
                            $infographic_source = get_field('infographic_source', $info_cpt_id); ?>
                            <div class="srm-table__source">
                                <?php printf(esc_html__( 'Source: ', 'srm')); ?>
                                <a href="<?= $infographic_source['url']; ?>" target="_blank"><?= $infographic_source['name']; ?></a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            <?php else : 
                // Side by side ?>
                <div class="srm-figure">
                    <div class="srm-figure__left">
                        <?php // GOES HERE
                        if ($infographic_type === 'infographic-custom') :
                            $info_image = get_sub_field('custom_infographic');
                            $info_image_mobile = get_sub_field('custom_infographic_mobile'); ?>
                            <div class="srm-table__image rounded">
                                <?php if ($info_image_mobile) : ?>
                                    <img src="<?= $info_image_mobile['url']; ?>" alt="<?= $info_image_mobile['alt']; ?>"/>
                                <?php else : ?>
                                    <img src="<?= $info_image['url']; ?>" alt="<?= $info_image['alt']; ?>"/>
                                <?php endif; ?>
                                <?php /* <img src="<?= $info_image['url']; ?>" alt="<?= $info_image['alt']; ?>" loading="lazy"/> */ ?>
                            </div>
                        <?php elseif ($infographic_type === 'infographic-cpt') :
                            $info_cpt_id = get_sub_field('infographic');
                            $info_image = get_field('infographic_image', $info_cpt_id);
                            $info_image_mobile = get_field('infographic_image_mobile', $info_cpt_id) ?>
                        
                            <div class="srm-table__image rounded">
                                <?php if ($info_image_mobile) : ?>
                                    <img class="srm-table__image--mobile" src="<?= $info_image_mobile['url']; ?>" alt="<?= $info_image_mobile['alt']; ?>" loading="lazy"/>
                                    <img class="srm-table__image--desktop" src="<?= $info_image['url']; ?>" alt="<?= $info_image['alt']; ?>" loading="lazy"/>
                                <?php else : ?>
                                    <img src="<?= $info_image['url']; ?>" alt="<?= $info_image['alt']; ?>" loading="lazy"/>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="srm-figure__right">
                        <div>
                            <?php if ($infographic_title) : ?>
                                <h4><?= $infographic_title; ?></h4>
                                
                            <?php endif; ?>
                            <?php if ($infographic_caption) : ?>
                                <?= $infographic_caption; ?>
                            <?php endif; ?>

                            <?php if (get_sub_field('include_source')) : ?>
                                <?php printf(esc_html__( 'Source: ', 'srm')); ?><a href="<?= $infographic_source['url']; ?>" target="_blank"><?= $infographic_source['name']; ?></a>    
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>