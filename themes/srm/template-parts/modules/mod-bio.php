<?php $post = get_sub_field('person'); ?>                
<?php if( $post ): ?>
    <section class="section small-margin">
        <div class="container">
            <div class="container__inner">
                <div class="text-wrapper bio-box">
                    <?php setup_postdata($post); ?>
                    <div class="bio-box__inner">
                        <div class="bio-box__inner--left">
                            <div class="bio-info">
                                <div class="bio-info__left">
                                    <?= get_the_post_thumbnail(get_the_ID(), 'thumbnail'); ?>
                                </div>
                                <div class="bio-info__right">
                                    <h3 class="bio-name"><?php the_title(); ?></h3>
                                    <?php if (get_field('job_title')) : ?>
                                        <h4 class="bio-title"><?= get_field('job_title'); ?></h4>
                                    <?php endif; ?>
                                    <?php if (get_field('organization')) : ?>
                                        <h4 class="bio-org"><?= get_field('organization'); ?></h4>
                                    <?php endif; ?>
                                </div>
                            </div>    
                        </div>
                        <div class="bio-box__inner--right bio-about">
                        
                            
                            <?php if (get_field('short_bio')) : ?>
                                <div class="bio-bio"><?= get_field('short_bio'); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>