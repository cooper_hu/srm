<?php // fields
$image_title = get_sub_field('image_title');
$image_description = get_sub_field('image_description');
$image_source = get_sub_field('image_source');
$image_size = get_sub_field('size'); ?>

<section class="section">
    <div class="container">
        <div class="<?php  if ($image_size === 'small' || $image_size === 'small-side-by-side' ) { echo 'container__inner'; } else { echo 'container__inner-wide'; } ?>">
            <?php if ($image_size === 'large' || $image_size === 'small' ) : ?>
                <div class="srm-table">
                    <?php
                    $desktop_image = get_sub_field('desktop_image');
                    $mobile_image = get_sub_field('mobile_image'); ?>
                    
                    <div class="srm-table__image">
                        <?php if ($mobile_image) : ?>
                            <img class="srm-table__image--mobile" src="<?= $mobile_image['url']; ?>" alt="<?= $mobile_image['alt']; ?>"/>
                            <img class="srm-table__image--desktop" src="<?= $desktop_image['url']; ?>" alt="<?= $desktop_image['alt']; ?>"/>
                        <?php else : ?>
                            <img src="<?= $desktop_image['url']; ?>" alt="<?= $desktop_image['alt']; ?>"/>
                        <?php endif; ?>
                    </div>
        
                    <?php if ($image_description) : ?>
                        <div class="srm-table__caption">
                            <?= $image_description; ?>
                        </div>
                    <?php endif; ?>

                    <?php if (get_sub_field('include_source')) : ?>
                        <div class="srm-table__source">
                            <?php // printf(esc_html__( 'Source: ', 'srm')); ?>
                            <a href="<?= $image_source['url']; ?>" target="_blank"><?= $image_source['name']; ?></a>
                        </div>
                    <?php endif; ?>

                </div>
            <?php else : 
                // Side by side ?>
                <div class="srm-figure">
                    <div class="srm-figure__left">

                        <?php
                        $desktop_image = get_sub_field('desktop_image');
                        $mobile_image = get_sub_field('mobile_image'); ?>
                        
        

                        <div class="srm-table__image rounded">
                            <?php if ($mobile_image) : ?>
                                <img class="srm-table__image--mobile" src="<?= $mobile_image['url']; ?>" alt="<?= $mobile_image['alt']; ?>"/>
                                <img class="srm-table__image--desktop" src="<?= $desktop_image['url']; ?>" alt="<?= $desktop_image['alt']; ?>"/>
                            <?php else : ?>
                                <img src="<?= $desktop_image['url']; ?>" alt="<?= $desktop_image['alt']; ?>"/>
                            <?php endif; ?>
                            <?php /* <img src="<?= $info_image['url']; ?>" alt="<?= $info_image['alt']; ?>" loading="lazy"/> */ ?>
                        </div>                
                    </div>
                    <div class="srm-figure__right">
                        <div>
                            <?php if ($image_title) : ?>
                                <h4><?= $image_title; ?></h4>
                            <?php endif; ?>
                            <?php if ($image_description) : ?>
                                <?= $image_description; ?>
                            <?php endif; ?>

                            <?php if (get_sub_field('include_source')) : ?>
                                <?php // printf(esc_html__( 'Source: ', 'srm')); ?>
                                <a href="<?= $image_source['url']; ?>" target="_blank"><?= $image_source['name']; ?></a>    
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>