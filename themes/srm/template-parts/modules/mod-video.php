<?php // fields
$video_title = get_sub_field('video_title');
$video_description = get_sub_field('video_description');
$video_type = get_sub_field('type'); ?>


<section class="section">
    <div class="container">
        <div class="container__inner">
            <?php if ($video_title) : ?>
                <h3><?= $video_title; ?></h3>
            <?php endif; ?>

            <?php if ($video_description) : ?>
                <div><?= $video_description; ?></div>
            <?php endif; ?>

            <?php if ($video_type === 'video-cpt') : 
                $video_cpt_id = get_sub_field('video'); 
                $cover_image = get_field('cover_image', $video_cpt_id); 
                $video = get_field('video', $video_cpt_id); 
                // Use preg_match to find iframe src.
                preg_match('/src="(.+?)"/', $video, $matches);
                $video_url = $matches[1]; ?>
                <a href="<?= $video_url; ?>" class="article-video-player js-play-video">
                    <img src="<?= $cover_image['url']; ?>" alt="<?= $cover_image['alt']; ?>"/>
                </a>    
            <?php elseif ($video_type === 'video-custom') : 
                $cover_image = get_sub_field('cover_image'); 
                $video = get_sub_field('custom_video'); 
                // Use preg_match to find iframe src.
                preg_match('/src="(.+?)"/', $video, $matches);
                $video_url = $matches[1]; ?>
                <a href="<?= $video_url; ?>" class="article-video-player js-play-video">
                    <img src="<?= $cover_image['url']; ?>" alt="<?= $cover_image['alt']; ?>"/>
                </a>    
            <?php endif; ?>        
        </div>
    </div>
</section>