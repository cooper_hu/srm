<?php // Fields
$image_position = get_sub_field('image_position');
$image = get_sub_field('image');
$content = get_sub_field('content'); 
$type = get_sub_field('media_type');
 
$container_width = get_sub_field('container_width');
$media_heading = get_sub_field('media_heading');
$border_style = get_sub_field('border_style'); ?>

<section class="section small-margin">
    <div class="container">
        <div class="<?= $container_width; ?>">
            <div class="col-split srm-image-text <?php if ($image_position === 'image-right') { echo 'flipped'; } ?>">
                <div class="col-split__left vertical-align-center">
                    <div class="border-wrapper <?= $type; ?>  <?= $border_style; ?> <?php if ($image_position === 'image-right') { echo 'flipped'; } ?>">
                        <div class="graphic-heading <?= $type; ?>  <?php if ($image_position === 'image-right') { echo 'flipped'; } ?>">
                            <?php if ($media_heading['headline']) : ?>
                                <h3><?= $media_heading['headline']; ?></h3>
                            <?php endif; ?>
                            <?php if ($media_heading['subhead']) : ?>
                                <?= str_replace( array('<p', '/p>'), array('<h4', '/h4>'), $media_heading['subhead'] ); ?>
                            <?php endif; ?>
                        </div>

                        <?php if ($type === 'infographic') : ?>
                            <div class="srm-image-text__infographic">
                                <?php $ai2html_url = get_sub_field('ai2html_url');
                                $aiurl =  parse_url($ai2html_url, PHP_URL_PATH); ?>
                                <div class="">
                                    <?php include ltrim($aiurl, '/');  ?>
                                </div>
                            </div>

                            <?php if (get_sub_field('include_source')) : 
                                $infographic_source = get_sub_field('infographic_source'); ?>
                                <div class="srm-table__source">
                                    <?php if ($infographic_source['url']) : ?>
                                        <a href="<?= $infographic_source['url']; ?>" target="_blank">
                                            <?= $infographic_source['name']; ?>
                                        </a>
                                    <?php else : ?>
                                        <?= $infographic_source['name']; ?>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>

                            <?php $download_link = get_sub_field('downloadable_graphic_url'); 
                            if ($download_link) : ?>
                                <div class="srm-image-text__download">
                                    <a href="<?= $download_link; ?>" download>
                                        <?php printf(esc_html__( 'Download Graphic', 'srm')); ?>
                                    </a>
                                </div>
                            <?php endif; ?>
                        <?php else : ?>
                            <div>
                                <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>
                            </div>
                            <?php $image_source = get_field('image_source', $image['ID']);
                            if ($image['caption'] || $image_source) : ?>
                                <p class="srm-image-text__caption small">
                                    <?= $image['caption']; ?>
                                    <?php if ($image_source) : ?>
                                        <a href="<?= $image_source['url']; ?>" target="<?= $image_source['target']; ?>"><?= $image_source['title']; ?></a>
                                    <?php endif; ?>
                                </p>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-split__right">
                    <div><?= $content; ?></div>
                </div>
            </div>
        </div>
    </div>
</section>