<?php // fields
$table_headline = get_sub_field('table_headline');
$table_header = get_sub_field('table_header');
$table_caption = get_sub_field('table_caption');
$table_source = get_sub_field('table_source');


$column_style = get_sub_field('column_style');
$column_amount = get_sub_field('column_amount');
$column_space = get_sub_field('column_space'); ?>

<section class="section">
    <div class="container">
        <div class="container__inner-wide">
            <div class="srm-table">
                <div class="srm-table__header">
                    <?php if ($table_headline) : ?>
                        <h3><?= $table_headline; ?></h3>
                    <?php endif; ?>
                </div>
                <?php if( have_rows('table_rows') ): ?>

                    <div class="srm-table__body">
                        <table class="<?= 'col-'.$column_amount; ?> <?= $column_style; ?> <?= $column_space; ?>">
                            
                            <?php if ($table_header['col_1'] || $table_header['col_2'] || $table_header['col_3']) : ?>
                                <thead>
                                    <tr>
                                        <th><?= $table_header['col_1']; ?></th>
                                        <th><?= $table_header['col_2']; ?></th>
                                        <?php if ($column_amount === 'three' || $column_amount === 'four' || $column_amount === 'five') : ?>
                                            <th><?= $table_header['col_3']; ?></th>
                                        <?php endif; ?>
                                        <?php if ($column_amount === 'four' || $column_amount === 'five') : ?>
                                            <th><?= $table_header['col_4']; ?></th>
                                        <?php endif; ?>
                                        <?php if ($column_amount === 'five') : ?>
                                            <th><?= $table_header['col_5']; ?></th>
                                        <?php endif; ?>
                                    </tr>
                                </thead>
                            <?php endif; ?>

                            <tbody>
                                <?php while ( have_rows('table_rows') ) : the_row(); ?>
                                    <tr>
                                        <td><?= get_sub_field('col_1'); ?></td>
                                        <td><?= get_sub_field('col_2'); ?></td>
                                        <?php if ($column_amount === 'three' || $column_amount === 'four' || $column_amount === 'five') : ?>
                                            <td><?= get_sub_field('col_3'); ?></td>
                                        <?php endif; ?>
                                        <?php if ($column_amount === 'four' || $column_amount === 'five') : ?>
                                            <td><?= get_sub_field('col_4'); ?></td>
                                        <?php endif; ?>
                                        <?php if ($column_amount === 'five') : ?>
                                            <td><?= get_sub_field('col_5'); ?></td>
                                        <?php endif; ?>
                                    </tr>
                                <?php endwhile; ?>
                            </tbody>
                        </table>    
                    </div>
                <?php endif; ?>

                <div class="srm-table__caption">
                    <?php if ($table_caption) : ?>
                        <p><?= $table_caption; ?></p>
                    <?php endif; ?>
                </div>

                <div class="srm-table__source">
                    <?php if (get_sub_field('include_source')) : ?>
                        <?php // printf(esc_html__( 'Source: ', 'srm')); ?>
                        <?php if ($table_source['url']) : ?>
                            <a href="<?= $table_source['url']; ?>" target="_blank"><?= $table_source['name']; ?></a> 
                        <?php else : ?>
                            <span><?= $table_source['name']; ?></span>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php /* <span data-html2canvas-ignore="true"></span> <a data-html2canvas-ignore href="#" class="js-download-table" data-title="SRM360 Table - <?= $table_headline; ?>"><?php printf(esc_html__( 'Download Table', 'srm')); ?></a> */ ?>
                </div>

            </div>
        </div>
    </div>
</section>