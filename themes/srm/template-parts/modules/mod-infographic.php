<?php // fields
$infographic_obj_id = get_sub_field('infographic_obj');
$infographic_title = get_sub_field('infographic_title');
$infographic_description = get_sub_field('infographic_description');
$infographic_layout = get_sub_field('layout'); 

// Infographic specific fields 
$infographic_type = get_sub_field('infographic_type');

if ($infographic_type === 'custom-infographic') {
    $infographic_size = get_sub_field('container_width');
} else {
    $infographic_size = get_field('infographic_size', $infographic_obj_id);
}

if (!isset($infographic_size)) {
    $infographic_size = 'container__inner-full'; // if not set
}

if ($infographic_layout === 'side-by-side') {
    $infographic_size = 'container__inner-wide'; // if not set
}
?>

<section class="section">
    <div class="container">
        <div class="<?= $infographic_size; ?>">            
            <div class="srm-table">
                <?php if ($infographic_title || $infographic_description ) : ?>
                    <div class="srm-table__caption infographic">
                        <h3><?= $infographic_title; ?></h3>
                        <?= $infographic_description; ?>
                    </div>
                <?php endif; ?>

                <?php if ($infographic_type === 'custom-infographic') : ?>
                    <?php $ai2html_url = get_sub_field('ai2html_url');
                    $aiurl = parse_url($ai2html_url, PHP_URL_PATH); ?>
                    <div class="srm-table__infographic">
                        <?php include ltrim($aiurl, '/');  ?>
                    </div>
                    <?php $download_link = get_sub_field('downloadable_graphic_url'); 
                    if ($download_link) : ?>
                        <a href="<?= $download_link; ?>" download>
                            <?php printf(esc_html__( 'Download Graphic', 'srm')); ?>
                        </a>
                    <?php endif; ?>
                    
           
                <?php else : ?>
                    <?php $ai2html_url = get_field('ai2html_url', $infographic_obj_id);
                    $aiurl = parse_url($ai2html_url, PHP_URL_PATH); ?>
                    <div class="srm-table__infographic">
                        <?php include ltrim($aiurl, '/');  ?>
                    </div>

                    <?php $download_link = get_field('downloadable_graphic', $infographic_obj_id); 
                    if ($download_link) : ?>
                    
                        <a href="<?= $download_link['url']; ?>" download>
                            <?php printf(esc_html__( 'Download Graphic', 'srm')); ?>
                        </a>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (get_field('include_source', $infographic_obj_id)) : 
                    $infographic_source = get_field('infographic_source', $infographic_obj_id); ?>
                    <div class="srm-table__source">
                        <?php if ($infographic_source['url']) : ?>
                            <a href="<?= $infographic_source['url']; ?>" target="_blank">
                                <?= $infographic_source['name']; ?>
                            </a>
                        <?php else : ?>
                            <?= $infographic_source['name']; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>