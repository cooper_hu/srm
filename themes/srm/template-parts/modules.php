<?php // Modules
if( have_rows('modules') ):
    while ( have_rows('modules') ) : the_row();
        if( get_row_layout() == 'mod_textblock' ):
            include get_template_directory() . '/template-parts/modules/mod-textblock.php'; 
        elseif( get_row_layout() == 'mod_box' ): 
            include get_template_directory() . '/template-parts/modules/mod-box.php'; 
        elseif( get_row_layout() == 'mod_image_text' ): 
            include get_template_directory() . '/template-parts/modules/mod-image-text.php'; 
        elseif( get_row_layout() == 'mod_image' ): 
            include get_template_directory() . '/template-parts/modules/mod-image.php';
        elseif( get_row_layout() == 'mod_infographic-obj' ): 
            include get_template_directory() . '/template-parts/modules/mod-infographic.php'; 
        elseif( get_row_layout() == 'mod_infographic' ): 
            include get_template_directory() . '/template-parts/modules/mod-infographic-old.php'; 
        elseif( get_row_layout() == 'mod_video' ): 
            include get_template_directory() . '/template-parts/modules/mod-video.php'; 
        elseif( get_row_layout() == 'mod_bio' ): 
            include get_template_directory() . '/template-parts/modules/mod-bio.php'; 
        elseif( get_row_layout() == 'mod_summary' ): 
            include get_template_directory() . '/template-parts/modules/mod-summary.php'; 
        elseif( get_row_layout() == 'mod_team' ): 
            include get_template_directory() . '/template-parts/modules/mod-team.php'; 
        elseif( get_row_layout() == 'mod_table' ): 
            include get_template_directory() . '/template-parts/modules/mod-table.php';
        elseif( get_row_layout() == 'mod_spacer' ): 
            include get_template_directory() . '/template-parts/modules/mod-spacer.php'; 
        endif;
    endwhile;
endif; ?>