<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SRM
 */

get_header();
?>

	<main id="primary" class="site-main">
        <section class="section">
            <div class="container">
                <div class="container__inner">
                    <div class="person-header">
                        <div class="person-header__top">
                            <div class="person-header__top--image">
                                <?= get_the_post_thumbnail(get_the_ID(), 'thumbnail'); ?>
                            </div>
                            <div class="person-header__top--content">
                                <div>
                                    <h1 class="person-name"><?php the_title(); ?></h1>
                                    <?php if (get_field('job_title')) : ?>
                                        <h2 class="person-title"><?= get_field('job_title'); ?></h2>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <?php if (get_field('bio')) : ?>
                            <div class="person-bio"><?= get_field('bio'); ?></div>
                        <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="section large-margin">
            <div class="container">
                <div class="container__inner-full">
                    
                    <?php // Content Written
                    $args = array(
                        'post_type' => array( 'perspective', 'article', 'infographic' ),
                        'posts_per_page' => -1,
                        'post_status'    => 'publish',
                        'meta_query'     => array(
                            array (
                                'key' => 'authors',
                                'value' => '"' . get_the_ID() . '"',
                                'compare' => 'LIKE'
                            )
                        )
                        // 'author' => $user->ID,
                    ); 
                    
                    $the_query = new WP_Query( $args ); ?>

                    <?php if ( $the_query->have_posts() ) : ?>
                        <div class="person-article-group">
                            <h2>Content Authored:</h2>
                            <div class="archive-grid">
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <?php get_template_part( 'template-parts/archive-card'); ?>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>

                    <?php // Content Reviewed 
                    $args = array(
                        'post_type' => array( 'perspective', 'article', 'video', 'podcast', 'infographic' ),
                        'posts_per_page' => -1,
                        'post_status'    => 'publish',
                        'meta_query'     => array(
                            array (
                                'key' => 'reviewers',
                                'value' => '"' . get_the_ID() . '"',
                                'compare' => 'LIKE'
                            )
                        )
                    ); 
                    
                    $the_query = new WP_Query( $args ); ?>

                    <?php if ( $the_query->have_posts() ) : ?>
                        <div class="section person-article-group">
                            <h2>Content Reviewed:</h2>
                            <div class="archive-grid">
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <?php get_template_part( 'template-parts/archive-card'); ?>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>

                    <?php // Content Featured In
                    $args = array(
                        'post_type' => array( 'perspective', 'article', 'video', 'podcast', 'infographic' ),
                        'posts_per_page' => -1,
                        'post_status'    => 'publish',
                        'meta_query'     => array(
                            array (
                                'key' => 'featured_people',
                                'value' => '"' . get_the_ID() . '"',
                                'compare' => 'LIKE'
                            )
                        )
                    ); 
                    
                    $the_query = new WP_Query( $args ); ?>

                    <?php if ( $the_query->have_posts() ) : ?>
                        <div class="section person-article-group">
                            <h2>Content Featured In:</h2>
                            <div class="archive-grid">
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <?php get_template_part( 'template-parts/archive-card'); ?>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>

	</main><!-- #main -->

<?php get_footer();
