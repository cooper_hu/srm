<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SRM
 */

get_header();
?>

	<main id="primary" class="site-main">
		<section class="section xs-margin">
			<div class="container">
				<div class="">
					<?php get_template_part( 'template-parts/search-bar'); ?>
				</div>
			</div>
		</section>

		<section class="section">
			<div class="container">
				<div class="container__inner-full">

					<?php if ( have_posts() ) : ?>
						<?php 
						global $wp_query;

						// Get the total number of posts
						$total_posts = $wp_query->found_posts; ?>

						<header class="archive-header">
							<div class="archive-header__content">
								<h1 class="h2">
									<?php post_type_archive_title(); ?>
									<?php printf(esc_html__( 'Archive', 'srm')); ?>
								</h1>
								<?php /* <h1 class="h2"><?= the_archive_title(); ?></h1> */ ?>
							</div>
							<div class="archive-header__sort">
								<div class="sort-filter-wrapper">
									<span><?php esc_html_e( 'Sort By', 'srm' ); ?>:</span>
									<select class="sort-filter js-sort-results" data-post-type="<?= $post->post_type; ?>" data-total-posts="<?= $total_posts; ?>">
										<option value="newest"><?php esc_html_e( 'Newest - Oldest', 'srm' ); ?></option>
										<option value="oldest"><?php esc_html_e( 'Oldest - Newest', 'srm' ); ?></option>
										<option value="ASC"><?php esc_html_e( 'A - Z', 'srm' ); ?></option>
										<option value="DESC"><?php esc_html_e( 'Z - A', 'srm' ); ?></option>
									</select>
								</div>
							</div>
						</header><!-- .page-header -->
						
						<div class="archive-grid">
							<?php while ( have_posts() ) : the_post();
								get_template_part( 'template-parts/archive-card');
							endwhile; ?>
						</div>

						<?php
						$search_results_per_page = get_option( 'posts_per_page' );
						
						if ($search_results_per_page < $total_posts) : ?>
							<div class="archive-grid-footer">
								<button class="btn blue-gradient wide js-load-more-search" 
									data-paged="1" 
									data-post-type="<?= $post->post_type; ?>"
									data-init-post-count="<?= $posts_per_page; ?>"
									data-total-posts="<?= $total_posts; ?>">
									Load More
								</button>
								<div class="loading-spinner" style="display: none;">Loading...</div>
							</div>
						<?php endif; ?>

						
					<?php endif; ?>
				</div>
			</div>
		</section>

	</main><!-- #main -->

<?php get_footer();
