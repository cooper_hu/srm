<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SRM
 */

?>

<footer class="site-footer">
	<div class="container">
		<div class="container__inner">
			<img class="site-footer-logo" src="<?= get_template_directory_uri(); ?>/assets/images/srm-logo-white.svg" alt="SRM Logo"/>
			<div class="site-footer-col">
				<div class="site-footer-col__left">
					<h4 class="subscribe-title"><?php printf(esc_html__( 'Subscribe', 'srm')); ?></h4>
					<p class="subscribe-message"><?php printf(esc_html__( 'Please subscribe to receive news and updates from SRM360. Your data will be held and used in line with our', 'srm')); ?> 
					<a href="/privacy-policy"><?php printf(esc_html__( 'Privacy Policy', 'srm')); ?></a>.</p>
					<div class="site-footer-newsletter">
						<?php get_template_part( 'template-parts/footer-newsletter'); ?>
						<?php // <div class="_form_3"></div><script src="https://srm360.activehosted.com/f/embed.php?id=3" charset="utf-8"></script> ?>
					</div>
				</div>
				<div class="site-footer-col__right">
					<h4><?php printf(esc_html__( 'Ask us a question!', 'srm')); ?></h4>
					<?php if (defined('ICL_SITEPRESS_VERSION')) {
						// WPML is active
						$current_language = apply_filters('wpml_current_language', null);

						if ($current_language === 'en') {
							$form_id = 2;
						} elseif ($current_language === 'es') {
							$form_id = 6;
						} elseif ($current_language === 'fr') {
							$form_id = 7;
						}
					} ?>
					
					<div class="site-footer-form">
						<?php echo FrmFormsController::get_form_shortcode( array( 'id' => $form_id ) ); ?>
					</div>

					<h4><?php printf(esc_html__( 'Follow Us', 'srm')); ?></h4>
					<div class="site-footer-socials">
						<a href="https://x.com/SRM360_org" target="_blank">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--!Font Awesome Free 6.6.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M389.2 48h70.6L305.6 224.2 487 464H345L233.7 318.6 106.5 464H35.8L200.7 275.5 26.8 48H172.4L272.9 180.9 389.2 48zM364.4 421.8h39.1L151.1 88h-42L364.4 421.8z"/></svg>
						</a>
						<a href="https://www.linkedin.com/company/srm360-org" target="_blank">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--!Font Awesome Free 6.6.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M100.3 448H7.4V148.9h92.9zM53.8 108.1C24.1 108.1 0 83.5 0 53.8a53.8 53.8 0 0 1 107.6 0c0 29.7-24.1 54.3-53.8 54.3zM447.9 448h-92.7V302.4c0-34.7-.7-79.2-48.3-79.2-48.3 0-55.7 37.7-55.7 76.7V448h-92.8V148.9h89.1v40.8h1.3c12.4-23.5 42.7-48.3 87.9-48.3 94 0 111.3 61.9 111.3 142.3V448z"/></svg>
						</a>

						<a href="https://bsky.app/profile/srm360.bsky.social" target="_blank">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--!Font Awesome Free 6.7.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M111.8 62.2C170.2 105.9 233 194.7 256 242.4c23-47.6 85.8-136.4 144.2-180.2c42.1-31.6 110.3-56 110.3 21.8c0 15.5-8.9 130.5-14.1 149.2C478.2 298 412 314.6 353.1 304.5c102.9 17.5 129.1 75.5 72.5 133.5c-107.4 110.2-154.3-27.6-166.3-62.9l0 0c-1.7-4.9-2.6-7.8-3.3-7.8s-1.6 3-3.3 7.8l0 0c-12 35.3-59 173.1-166.3 62.9c-56.5-58-30.4-116 72.5-133.5C100 314.6 33.8 298 15.7 233.1C10.4 214.4 1.5 99.4 1.5 83.9c0-77.8 68.2-53.4 110.3-21.8z"/></svg>
						</a>

						<a href="https://www.youtube.com/@SRM360_org" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!--!Font Awesome Free 6.7.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2025 Fonticons, Inc.--><path d="M549.7 124.1c-6.3-23.7-24.8-42.3-48.3-48.6C458.8 64 288 64 288 64S117.2 64 74.6 75.5c-23.5 6.3-42 24.9-48.3 48.6-11.4 42.9-11.4 132.3-11.4 132.3s0 89.4 11.4 132.3c6.3 23.7 24.8 41.5 48.3 47.8C117.2 448 288 448 288 448s170.8 0 213.4-11.5c23.5-6.3 42-24.2 48.3-47.8 11.4-42.9 11.4-132.3 11.4-132.3s0-89.4-11.4-132.3zm-317.5 213.5V175.2l142.7 81.2-142.7 81.2z"/></svg>
						</a>
					</div>
				</div>
			</div>

			<div class="site-footer__legal">
				<p><?php printf(esc_html__( 'SRM360 supports an informed, evidence-based discussion of sunlight reflection methods (SRM), otherwise known as Solar Radiation Modification (SRM) or solar geoengineering, by synthesising, explaining, and communicating the science in a clear and accessible way.', 'srm')); ?></p>

				<?php wp_nav_menu(
				array(
					'theme_location' => 'legal-menu',
				)
			); ?>
			</div>
		</div>
	</div>
</footer>
	
<?php wp_footer(); ?>

<?php // Linkedin Insight Tag ?>
<script type="text/javascript"> 
	_linkedin_partner_id = "6548650";
	window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
	window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script>
<script type="text/javascript">
	(function(l) {
		if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])};
		window.lintrk.q=[]}
		var s = document.getElementsByTagName("script")[0];
		var b = document.createElement("script");
		b.type = "text/javascript";b.async = true;
		b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
		s.parentNode.insertBefore(b, s);})(window.lintrk);
</script>
<noscript>
	<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=6548650&fmt=gif" />
</noscript>

</body>
</html>
