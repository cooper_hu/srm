<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SRM
 */

get_header();

$alphabet = range('A', 'Z');

?>

	<main id="primary" class="site-main">
		<section class="section xs-margin">
			<div class="container">
				<div class="">
					<?php get_template_part( 'template-parts/search-bar'); ?>
				</div>
			</div>
		</section>

		<section class="section">
			<div class="container">
				<div class="container__inner">

                <?php 
                $args = array(
                    'post_type' => 'glossary',  // Replace with your post type
                    'posts_per_page' => -1,
                    'orderby'   => 'title',
                    'order'     => 'ASC',   // Use 'DESC' for reverse alphabetical order
                );

                $query = new WP_Query( $args );

                $current_letter = ''; // To track the current letter group

                if ( $query->have_posts() ) : ?>
                    <div class="glossary-nav">
                        <?php // Glossary 
                        $active_letters = array();
                        // grab active letters
                        while ( $query->have_posts() ) { 
                            $query->the_post();
                            $first_letter = strtoupper( substr( get_the_title(), 0, 1 ) );
                            if ( $first_letter !== $current_letter ) {
                                $current_letter = $first_letter;
                                array_push($active_letters, $first_letter);
                            }      
                        } 
                        
                        // print out glossay key
                        foreach ($alphabet as $letter) {
                            if (in_array($letter, $active_letters)) {
                                echo '<a class="js-scroll-to-section" href="#section-' . $letter. '">' . $letter . '</a>';
                            } else {
                                echo '<span class="inactive-letter">' . $letter . '</span>';
                            }
                        } ?>
                    </div>




                    <div class="accordion-group">
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <?php $first_letter = strtoupper( substr( get_the_title(), 0, 1 ) );  // Get first letter of the title ?>
                            
                            <?php // Check if the current letter is different from the previous one
                            if ( $first_letter !== $current_letter ) : ?>
                                    <?php // If we're in a new letter section, close the previous section (if it's not the first)
                                    if ( $current_letter !== '' ) {
                                        echo '</div>';
                                    } ?>

                                    <?php // Start a new section for the current letter
                                    echo '<h2 id="section-' . $first_letter. '">' . $first_letter . '</h2>';
                                    echo '<div class="glossary-section">';
                                    
                                    $current_letter = $first_letter; ?>
                            <?php endif; ?>
                                <div class="accordion-item">
                                    <div class="accordion-item__header">
                                        <h3 class="css-no-margin"><?php the_title(); ?></h3>
                                    </div>
                                    <div class="accordion-item__content">
                                        <div class="accordion-item__content--inner">
                                            <?= get_field('definition'); ?>
                                        </div>
                                    </div>
                                </div>

                               <?php // echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>'; ?>

                            <?php /*
                            <div class="accordion-item">
                                <div class="accordion-item__header event">
                                    <h3><?php the_title(); ?></h3>
                                </div>
                                <div class="accordion-item__content">
                                    <div class="accordion-item__content--inner">
                                        <?= get_field('definition'); ?>
                                    </div>
                                </div>
                            </div>
                            */ ?>
                        <?php endwhile; ?>
                        <?php echo '</div>'; ?>
                    </div>
                <?php endif; 
                wp_reset_postdata(); ?>


					
				</div>
			</div>
		</section>

	</main><!-- #main -->

<?php get_footer();
