<?php
/**
 * SRM functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package SRM
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '0.9.3' );
}

function srm_setup() {
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );

	/** Register Navs */
	register_nav_menus(
		array(
			'main-menu-left' => esc_html__( 'Main Menu - Left', 'srm' ),
			'main-menu-right' => esc_html__( 'Main Menu - Right', 'srm' ),
			'content-menu' => esc_html__( 'Content Menu', 'srm' ),
			'legal-menu' => esc_html__( 'Legal Menu', 'srm' ),
		)
	);

	/** Theme Support */
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);
}

add_action( 'after_setup_theme', 'srm_setup' );

/* ************************************
 * Enqueue scripts and styles.
 * ************************************ */
function srm_scripts() {
	// Styles
	wp_enqueue_style( 'srm-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'srm-custom-style', get_template_directory_uri() . '/assets/css/main.css', array(), _S_VERSION );

	// Scripts
	wp_enqueue_script( 'ajax', get_stylesheet_directory_uri(). '/assets/js/js-bundle.js', array('jquery'), _S_VERSION, true );
    wp_localize_script( 'ajax', 'wp_ajax',
        array('ajax_url' => admin_url('admin-ajax.php'))
    );

	wp_enqueue_script( 'ai2html-resizer', get_stylesheet_directory_uri(). '/assets/js/ai2html-resizer.js', array('jquery'), _S_VERSION, false );


	
}
add_action( 'wp_enqueue_scripts', 'srm_scripts' );

/* ************************************
 * External Files
 * ************************************ */
require get_template_directory() . '/inc/custom-header.php';

/** Custom template tags for this theme. */
require get_template_directory() . '/inc/template-tags.php';

/** Functions which enhance the theme by hooking into WordPress. */
require get_template_directory() . '/inc/template-functions.php';

/** ACF Related functions */
require get_template_directory() . '/inc/acf-setup.php';

/** Meta Boxes */
require get_template_directory() . '/inc/metaboxes.php';

/** Meta Boxes */
require get_template_directory() . '/inc/filtering.php';

/** Relevanssi */
require get_template_directory() . '/inc/relevanssi.php';

/** Airtable */
require get_template_directory() . '/inc/airtable.php';