<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SRM
 */

get_header(); ?>

<main id="primary" class="site-main">

	<section class="section xs-margin">
		<div class="container">
			<div class="">
				<?php get_template_part( 'template-parts/search-bar'); ?>
			</div>
		</div>
	</section>

	<?php include get_template_directory() . '/template-parts/modules.php'; ?>	

</main>

<?php get_footer();
