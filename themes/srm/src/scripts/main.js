jQuery(function($) {
  /* ******************************************************
   * Sitewide Functions
   * ****************************************************** */
  initBase();
  initSiteNav();

  initVideos();
  initAccordions();
  initModules();

  if ($('body').hasClass('single-podcast')) {
    initSinglePodcast();
  }

  if ($('body').hasClass('search-results')) {
    initSearchPage();
  }

  if ($('body').hasClass('category')) {
    initCategoryArchive();
  }

  if ($('body').hasClass('post-type-archive')) {
    initArchive();
  }

  if ($('body').hasClass('page-template-tp-faq')) {
    initFAQ();
  }
});

function closeNav() {
  $('.js-open-search').removeClass('active');
  $('.nav-button').removeClass('active');
  $('body').removeClass('fixed');
  
  gsap.to(".navigation", {autoAlpha: 0, duration: 0.35});
  gsap.to(".search-popup", {autoAlpha: 0, duration: 0.35});
}

function initSiteNav() {  
  $('.js-open-nav').magnificPopup({
    type: 'inline',
		preloader: false,
    fixedContentPos: true,
    mainClass: 'mfp-fade mfp-nav'
  });

  $('.js-open-secondary-nav').magnificPopup({
    type: 'inline',
		preloader: false,
    fixedContentPos: true,
    mainClass: 'mfp-fade mfp-cat-nav'
  });

  $('.js-open-mobile-nav').magnificPopup({
    type: 'inline',
		preloader: false,
    fixedContentPos: true,
    mainClass: 'mfp-fade mfp-mobile-nav'
  });

  $('.js-close-nav').on('click', function() {
    $.magnificPopup.close();
  });

  $('.js-scroll-to-section').on('click', function(e) {
    e.preventDefault();
    var section = $(this).attr('href');
    smoothScrollToSection(section, 96);
  });

  $(window).on('load', function() {
    var scrollPosition = $(window).scrollTop();

    if ($(window).scrollTop() > 100) {
        $('.site-header').addClass('scrolled');
    } else {
        $('.site-header').removeClass('scrolled');
    }
  });

  $(document).ready(function() {
    $(window).trigger('load'); // Manually trigger the load event (to make sure nav fades in)
  });

  // Add color to nav on scroll
  $(window).scroll(function() {
    // console.log($(this).scrollTop());
    if ($(this).scrollTop() > 100) {
       $('.site-header').addClass('scrolled');
    } else {
       $('.site-header').removeClass('scrolled');
    }
  });
}


function initBase() {
  $('.js-scroll-to').on('click', function(e) {
    e.preventDefault();
    let dest = $(this).attr('href');

    $('html, body').animate({
      scrollTop: $("#team").offset().top
    }, 600);
  });
  
  initAnimations();

  // Check if there is a hash in the URL
  if (window.location.hash) {
    // Set a timeout to allow the browser to scroll to the anchor
    setTimeout(function() {
      var target = $(window.location.hash);
      if (target.length) {
        // Calculate the offset
        var offset = target.offset().top - 96; // Adjust 50 to your desired offset
        // Scroll to the offset position
        $('html, body').animate({ scrollTop: offset }, 0);
      }
    }, 0); // Immediate execution after the page is loaded
  }
}

function initAnimations() {
  // general fade in and up
  ScrollTrigger.batch('.js-ani-fade-up', {
    onEnter: batch => gsap.to(batch, {y: 0, opacity: 1, autoAlpha: 1, stagger: 0.15})
  });
}

function initVideos() {
  $('.js-play-video').each(function() {
    let aspectRatio = $(this).data('size');
    
    $(this).magnificPopup({
      type: 'iframe',
      mainClass: 'mfp-fade mfp-video',
      removalDelay: 160,
      preloader: false,
      fixedContentPos: true,
      iframe: {
        markup: '<div class="mfp-iframe-scaler ' + aspectRatio + '">' +
          '<div id="iframeId" class="mfp-close"></div>' +
          '<iframe class="mfp-iframe" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>' +
          '</div>' +
          '<div class="mfp-bottom-bar">' +
            '<div class="mfp-title"></div>' +
          '</div>',
        patterns: {
          vimeo: {
              index: 'vimeo.com/', 
              id: function(url) {        
                  var m = url.match(/(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/);
                  if ( !m || !m[5] ) return null;
                  return m[5];
              },
              src: 'https://player.vimeo.com/video/%id%?autoplay=1'
          },
          youtube: {
            index: 'youtube.com/',
            id: function (url) { return url },
            src: '%id%&autoplay=1'
          },
        }
      }
    });
  });
}

function initAccordions() {
  $('.accordion-item__header').on('click', function() {
    var parent = $(this).parent();
    parent.toggleClass('clicked');

    /* Closes All Others
    $('.accordion-item').each(function() {
      if (!$(this).hasClass('clicked')) {
        $(this).removeClass('active');
        gsap.to($(this).find('.accordion-item__content'), 0.35, {height: 0});
      }    
    });
    */

    var parent = $(this).parent();
    
    parent.toggleClass('active');
    parent.toggleClass('clicked');

    gsap.to(parent.find('.accordion-item__content'), 0.35, {height: 0});

    if (parent.hasClass('active')) {
      gsap.to(parent.find('.accordion-item__content'), 0.35, {height: 'auto'});
    }
  });
}

/* Cat Archive Page */

/* ******************************************************
  * Category Archive Page
  * ****************************************************** */
function initCategoryArchive() {
  $('.js-load-more-cat').on('click', function() {
    var button = $(this);
    var paged = button.data('paged'); // Get the next page
    var postType = button.data('post-type'); // Get the post type
    var catID = button.data('category-id'); 
    var excludedIDs = button.data('excluded-ids'); 
    var totalPosts = button.data('total-posts');
    var initPosts = button.data('init-post-count');

    // alert('hi');

    var sendData= {
      action: 'load_more_posts',
      paged: paged,
      post_type: postType,
      cat_id: catID,
      excluded_ids: excludedIDs
    };

    console.log('data', sendData);
    
    $.ajax({
        url: wp_ajax.ajax_url,
        type: 'POST',
        data: sendData,
        beforeSend: function() {
            button.siblings('.loading-spinner').show(); // Show loading spinner
            button.hide();
        },
        success: function(response) {
          console.log(response);

          if (response) {
            console.log(response);

            // Append new posts to the list
            
            $('.archive-grid-sm.' + postType).append(response);
            
            // Determine if load more button needs to be hidden
            var visibleCount = $('.archive-grid-sm.' + postType).children().length + initPosts; // account for first 3

            button.data('paged', paged + 1);

            // Hide Loading Icon
            button.siblings('.loading-spinner').hide(); // Hide loading spinner
            
            if (visibleCount >= totalPosts) {
              button.hide();
            } else {
              button.show();
            }
          }
        },
        error: function(response) {
          console.log('error', response);
        }
    });
    
  });
}

/* ******************************************************
  * Single Page (Podcast)
  * ****************************************************** */
function initSinglePodcast() {
  // alert('on pdocast');
  var mp3 = $('#single-song-player').data('mp3');

  Amplitude.init({
    "bindings": {
      37: 'prev',
      39: 'next',
      32: 'play_pause'
    },
    "songs": [
      {
        "url": mp3
        // "url": "https://521dimensions.com/song/Ancient Astronauts - Risin' High (feat Raashan Ahmad).mp3",
      }
    ]
  });

  window.onkeydown = function(e) {
      return !(e.keyCode == 32);
  };

  document.getElementById('song-played-progress').addEventListener('click', function( e ){
    var offset = this.getBoundingClientRect();
    var x = e.pageX - offset.left;

    Amplitude.setSongPlayedPercentage( ( parseFloat( x ) / parseFloat( this.offsetWidth) ) * 100 );
  });
}

/* ******************************************************
  * Search Results Page
  * ****************************************************** */
var page = 2; // start on page 2 for search results next page

function initSearchPage() {
  // Filter Search Results
  $('.js-filter-search-results').on('click', function(e) {
    e.preventDefault();

    var currentBtn = $(this);
    var postType = currentBtn.data('post-type'); // Get the post type
    var postTypeCount = currentBtn.data('post-count');
    var searchTerm = $('.category-subnav').data('search-term');
    
    var loadMoreBtn = $('.js-load-more-search');
    var totalPosts = loadMoreBtn.data('total-posts');
    var initPosts = loadMoreBtn.data('init-post-count');

    console.log('post', postType);

    var sendData = {
      action: 'filter_search_results',
      post_type: postType,
      search_term: searchTerm
    };

    $.ajax({
      url: wp_ajax.ajax_url,
      type: 'POST',
      data: sendData,
      beforeSend: function() {
        $('.archive-grid').html();
        loadMoreBtn.siblings('.loading-spinner').show(); // Show loading spinner
        loadMoreBtn.hide();
      },
      success: function(response) {
        if (response) {
          $('.category-subnav a').removeClass('active');
          currentBtn.addClass('active');
          loadMoreBtn.siblings('.loading-spinner').hide();

          // console.log(response);
          $('.archive-grid').html(response);

          loadMoreBtn.data('post-type', postType);
          loadMoreBtn.data('total-posts', postTypeCount);

          page = 2;

          // console.log('new total posts in filter', postTypeCount);

          var visibleCount = $('.archive-grid').children().length; // account for first 3
          
          console.log('postTypecount', postTypeCount);
          console.log('visibleCount', visibleCount);
          

          if (postTypeCount > visibleCount) {
            loadMoreBtn.show();
          } else {
            loadMoreBtn.hide();
          }
        }
      }
    });
  });

  // Load More Search Results
  $('.js-load-more-search').on('click', function(e) {
    e.preventDefault();

    var searchTerm = $('.category-subnav').data('search-term');
    var loadMoreBtn = $(this);
   
    var initPosts = loadMoreBtn.data('init-post-count');
    var totalPosts = loadMoreBtn.data('total-posts');
    var postType = loadMoreBtn.data('post-type');
    var sortValue = $('.js-sort-results').val();
    
    var sendData = {
      action: 'load_more_results',
      post_type: postType,
      search_term: searchTerm,
      page: page,
      sort_by: sortValue,
    };

    loadMorePosts(sendData, loadMoreBtn, totalPosts);
  });

  $('.js-sort-results').on('change', function() {
    var searchTerm = $('.category-subnav').data('search-term');

    var selectedValue = this.value;
    var loadMoreBtn = $('.js-load-more-search');

    var initPosts = $(this).data('init-post-count');
    var totalPosts = $(this).data('total-posts');
    var postType = $(this).data('post-type');
    var sortValue = $('.js-sort-results').val();
    
    var sendData = {
      action: 'sort_post_results',
      post_type: postType,
      search_term: searchTerm,
      page: page,
      sort_by: selectedValue,
    };

    console.log('send', sendData);

    sortPosts(sendData, loadMoreBtn, totalPosts);
  });
}

/* FAQ Page */
function initFAQ() {
  
  $('.js-filter-faq').on('change', function() {
    var selectedValue = this.value;

    var sendData = {
      action: 'filter_faq',
      sort_by: selectedValue,
    };

    if (selectedValue === 'featured') {
      $('.js-featured-faqs').css('display', 'block');
      $('.js-filtered-faqs').css('display', 'none');
    } else {
      console.log('faq', sendData);
      $('.js-featured-faqs').css('display', 'none');
      $('.js-filtered-faqs').css('display', 'block');

      $.ajax({
        url: wp_ajax.ajax_url,
        type: 'POST',
        data: sendData,
        success: function(response) {
          if (response) {
            $('.js-filtered-faqs').html(response);
            initAccordions();
          }
        }
      });
    }
  });
}

/* ******************************************************
  * Archive Pages
  * ****************************************************** */
var page = 2; // start on page 2 for search results next page

function initArchive() {
  console.log('init archive');

  // filter current results
  $('.js-sort-results').on('change', function() {
    var selectedValue = this.value;
    var loadMoreBtn = $('.js-load-more-search');
    var totalPosts = $(this).data('total-posts');
    var postType = $(this).data('post-type');
    
    var sendData = {
      action: 'sort_post_results',
      post_type: postType,
      page: page,
      sort_by: selectedValue,
    };

    console.log('change', sendData);

    sortPosts(sendData, loadMoreBtn, totalPosts);
  });

  // Load More Search Results
  $('.js-load-more-search').on('click', function(e) {
    e.preventDefault();

    var loadMoreBtn = $(this);
    var initPosts = loadMoreBtn.data('init-post-count');
    var totalPosts = loadMoreBtn.data('total-posts');
    var postType = loadMoreBtn.data('post-type');
    var sortValue = $('.js-sort-results').val();
    
    var sendData = {
      action: 'load_more_results',
      post_type: postType,
      page: page,
      sort_by: sortValue,
    };

    console.log('obj', sendData);

    loadMorePosts(sendData, loadMoreBtn, totalPosts);
  });
}


/* ******************************************************
  * Modules
  * ****************************************************** */
function initModules() {
  $('.js-download-table').on('click', function(e) {
    e.preventDefault();

    var button = $(this);

    myFunction(button);
  });
}

async function myFunction(button) {
  const originalViewport = document.querySelector('meta[name="viewport"]').getAttribute('content');
  const viewportMeta = document.querySelector('meta[name="viewport"]');
  viewportMeta.setAttribute('content', 'width=1440');

  // Wait for the viewport to adjust
  await new Promise(resolve => setTimeout(resolve, 300));

  var element2 = button.parent().parent();
  var fileName = button.data('title');

  console.log("3002ms have passed");
  
  html2canvas(element2[0], { 
    scale: 2 
  }).then(function(canvas) {
      var link = document.createElement('a');
      link.href = canvas.toDataURL('image/png');
      link.download = fileName + '.png'; // Set download filename
      link.click(); // Trigger download
      
      // Wait for the viewport to adjust back
      // 
  });

  viewportMeta.setAttribute('content', originalViewport);
  await new Promise(resolve => setTimeout(resolve, 300));
}

async function downloadImage(button) {
  // await new Promise(resolve => setTimeout(resolve, 300));
  // console.log("300ms have passed");

  
  var element2 = button.parent().parent();
  var fileName = button.data('title');

    var originalViewport = document.querySelector('meta[name="viewport"]').getAttribute('content');
    var viewportMeta = document.querySelector('meta[name="viewport"]');

    viewportMeta.setAttribute('content', 'width=1440');
  
    // Wait for the viewport to adjust
    // await new Promise(resolve => setTimeout(resolve, 300));

    // button.addClass('hidden');

    html2canvas(element2[0], { 
      scale: 2 
    }).then(function(canvas) {
        var link = document.createElement('a');
        link.href = canvas.toDataURL('image/png');
        link.download = fileName + '.png'; // Set download filename
        link.click(); // Trigger download
        viewportMeta.setAttribute('content', originalViewport);
        // Wait for the viewport to adjust back
        // await new Promise(resolve => setTimeout(resolve, 300));
    });
}

/*

// Temporarily set the viewport to a larger size for capturing the content
    const originalViewport = document.querySelector('meta[name="viewport"]').getAttribute('content');
    const viewportMeta = document.querySelector('meta[name="viewport"]');
    viewportMeta.setAttribute('content', 'width=1440');
  
    // Wait for the viewport to adjust
    await new Promise(resolve => setTimeout(resolve, 300));
  
    const element = contentRef.current;
    const canvas = await html2canvas(element, { scale: 2 });
    const imgData = canvas.toDataURL('image/png');
    const imgBlob = await fetch(imgData).then(res => res.blob());
    const file = new File([imgBlob], 'document.png', { type: 'image/png' });
  
    // Restore the original viewport
    viewportMeta.setAttribute('content', originalViewport);
    // Wait for the viewport to adjust back
    await new Promise(resolve => setTimeout(resolve, 300));
    return file;





var scaleBy = 5;
    var w = 1000;
    var h = 1000;
    var div = document.querySelector('#screen');
    var canvas = document.createElement('canvas');
    canvas.width = w * scaleBy;
    canvas.height = h * scaleBy;
    canvas.style.width = w + 'px';
    canvas.style.height = h + 'px';
    var context = canvas.getContext('2d');
    context.scale(scaleBy, scaleBy);

    html2canvas(div, {
        canvas:canvas,
        onrendered: function (canvas) {
            theCanvas = canvas;
            document.body.appendChild(canvas);

            Canvas2Image.saveAsPNG(canvas);
            $(body).append(canvas);
        }
    });
    */ 
/* ******************************************************
  * Helper Functions
  * ****************************************************** */
function loadMorePosts(dataObject, loadMoreBtn, totalPosts) {
  $.ajax({
    url: wp_ajax.ajax_url,
    type: 'POST',
    data: dataObject,
    beforeSend: function() {
      loadMoreBtn.siblings('.loading-spinner').show(); // Show loading spinner
      loadMoreBtn.hide();
    },
    success: function(response) {
      if (response) {
        $('.archive-grid').append(response);

        page++; // add to the page count

        var visibleCount = $('.archive-grid').children().length; // account for first 3
        
        // Hide Loading Icon
        loadMoreBtn.siblings('.loading-spinner').hide(); // Hide loading spinner

        console.log('new total posts in load more', totalPosts);

        if (visibleCount >= totalPosts) {
          loadMoreBtn.hide();
        } else {
          loadMoreBtn.show();
        }
      }
    }
  });
}

function sortPosts(dataObject, loadMoreBtn, totalPosts) {
  $.ajax({
    url: wp_ajax.ajax_url,
    type: 'POST',
    data: dataObject,
    beforeSend: function() {
      loadMoreBtn.siblings('.loading-spinner').show(); // Show loading spinner
      loadMoreBtn.hide();
    },
    success: function(response) {
      if (response) {
        $('.archive-grid').html(response);

        var visibleCount = $('.archive-grid').children().length; // account for first 3
        
        // Hide Loading Icon
        loadMoreBtn.siblings('.loading-spinner').hide(); // Hide loading spinner

        // console.log('new total posts in load more', totalPosts);

        if (visibleCount >= totalPosts) {
          loadMoreBtn.hide();
        } else {
          loadMoreBtn.show();
        }
      }
    }
  });
}

function smoothScrollToSection(sectionId, offset = 0) {
  // Get the target section element
  const targetSection = document.querySelector(sectionId);

  if (targetSection) {
      // Calculate the position of the target section minus the offset (e.g., for sticky nav)
      const sectionPosition = targetSection.getBoundingClientRect().top + window.scrollY - offset;

      // Use window.scrollTo with smooth behavior
      window.scrollTo({
          top: sectionPosition,
          behavior: 'smooth'
      });
  }
}



/*
  if ($('body').hasClass('single-podcast')) {
    const podcastTitle = $('.single-media__cover').data('title');
    const podcastURL = $('.single-media__cover').data('mp3');

    // Podcast
    Amplitude.init({
      "preload": 'none',
      "bindings": {
        37: 'prev',
        39: 'next',
        // 32: 'play_pause'
      },
      "songs": [
        {
          "name": podcastTitle,
          "artist": "Climate Now",
          "url": podcastURL,
        }
      ]
    }); 

    $('#single-song-player').css('opacity', 1);

    /*
    window.onkeydown = function(e) {
        return !(e.keyCode == 32);
    };*

    document.getElementById('song-played-progress').addEventListener('click', function( e ){
      var offset = this.getBoundingClientRect();
      var x = e.pageX - offset.left;

      Amplitude.setSongPlayedPercentage( ( parseFloat( x ) / parseFloat( this.offsetWidth) ) * 100 );
    });

    $('.single-media__cover.single-page').on('click', function(e) {
      e.preventDefault();
      $(this).fadeOut();
      $('.amplitude-play-pause').click();

       
      var label = $('.single-title').html();
      // Event Tracking

      gtag('event', 'Download', {
        'event_category': 'Pocast',
        'event_label': label,
      });
    });
  }
  */