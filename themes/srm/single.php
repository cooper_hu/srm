<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SRM
 */

if (is_singular('glossary')) {
	// prevent single glossary pages from being accessible
	header( "Location: /glossary/" );
    exit;
}

get_header(); 

$citation_list = ' '; ?>

	<main id="primary" class="site-main">
		<section class="section xs-margin">
			<div class="container">
				<div class="">
					<?php get_template_part( 'template-parts/search-bar'); ?>
				</div>
			</div>
		</section>
		<article>
			<?php include get_template_directory() . '/template-parts/article-header.php'; ?>
			<?php include get_template_directory() . '/template-parts/article-media.php'; ?>
			
			<?php include get_template_directory() . '/template-parts/modules.php'; ?>
			<?php include get_template_directory() . '/template-parts/article-footer.php'; ?>
		</article>
	</main>

	<?php /* 
	<div class="container">
		<div class="container__inner">
			<div class="text-wrapper">
				<h1>Reflectance Spectra: Planet Earth</h1>
				<h2>Reflectance Spectra: Planet Earth</h2>
				<h3>Reflectance Spectra: Planet Earth</h3>
				<h4>Reflectance Spectra: Planet Earth</h4>
				<h5>Reflectance Spectra: Planet Earth</h5>
				<h6>Reflectance Spectra: Planet Earth</h6>
				<p>The ocean absorbs CO2 at its surface, taking in about 25% of human-caused CO2 emissions per year (Watson et al., 2020). The ocean's capacity to absorb CO2 makes it the second largest carbon reservoir after the solid earth (Devries, 2022).</p>
				<p>The ocean's ability to store CO2 has partially mitigated the warming effects of fossil fuel combustion (Devries, 2022). However, the accumulation of CO2 in the ocean since the start of the industrial revolution has affected ocean water chemistry and marine life (Error! Reference source not found.).</p>
			</div>
		</div>
	</div>
	*/ ?>

<?php get_footer();
