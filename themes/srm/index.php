<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SRM
 */

get_header(); ?>

<main id="primary" class="site-main">
	<section class="section">
		<div class="container">
			<div class="container__inner-wide">
				<br/><br/>
				<h2>Content:</h2>
				<?php
				$args = array(
					'post_type' => array( 'perspective', 'article', 'video', 'podcast', 'infographic' ),
					'posts_per_page' => -1,
				);

				$the_query = new WP_Query( $args ); ?>

				<?php if ( $the_query->have_posts() ) : ?>
					<div class="archive-grid">
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<?php get_template_part( 'template-parts/archive-card'); ?>
						<?php endwhile; ?>
					</div>

					<?php wp_reset_postdata(); ?>

				<?php else : ?>
					<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>

				<br/><br/><br/>

				<?php
				$args = array(
					'post_type' => array( 'person' ),
					'posts_per_page' => -1,
				);

				$the_query = new WP_Query( $args ); ?>

				<?php if ( $the_query->have_posts() ) : ?>
					
					<h2>People:</h2>
					<div class="archive-grid">

						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<div>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</div>
						<?php endwhile; ?>
					</div>

					<?php wp_reset_postdata(); ?>

				<?php else : ?>
					<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</section>

</main><!-- #main -->

<?php get_footer();
