<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SRM
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
                <div class="text-wrapper">
                    <?php
                    the_archive_title( '<h1 class="page-title">', '</h1>' );
                    the_archive_description( '<div class="archive-description">', '</div>' );
                    ?>
                </div>
			</header><!-- .page-header -->

            <p>Hi</p>
            <div class="container" style="outline: 1px solid red;">
            
                <?php 
                $args = array(
                    'post_type' => array( 'post' ),
                    'posts_per_page' => -1,
                    
                );
                $the_query = new WP_Query( $args );
                // The Loop.
                if ( $the_query->have_posts() ) : ?>
                    <div class="expert-card__list-wrapper">
                        <h5 class="css-gold">Featured In:</h5>
                        <ul class="expert-card__list">
                            <?php while ( $the_query->have_posts() ) {
                                $the_query->the_post();
                                echo '<li><a href="' . get_the_permalink(get_the_ID()) . '">' . ucfirst(get_post_type()) . ': ' .  esc_html( get_the_title(get_the_ID()) ) . '</a></li>';
                            } ?>
                        </ul>
                    </div>
                <?php endif; 
                wp_reset_postdata(); ?>
            </div>

		<?php endif; ?>

	</main><!-- #main -->

<?php get_footer();
