<?php 
/* ************************************
 * ACF Related Stuff
 * ************************************ */
// Lowers the metabox priority to 'core' for Yoast SEO's metabox.
function lower_yoast_metabox_priority( $priority ) {
	return 'core';
}

add_filter( 'wpseo_metabox_prio', 'lower_yoast_metabox_priority' );

// Custom WYSIWYG toolbars
function my_toolbars( $toolbars ) {
	$toolbars['Base'] = array();
	$toolbars['Base'][1] = array('formatselect', 'bold', 'underline', 'italic', 'bullist', 'numlist', 'subscript','superscript', 'link',  'undo', 'redo', 'fullscreen');

    $toolbars['Base (No Format)'] = array();
	$toolbars['Base (No Format)'][1] = array('bold', 'underline', 'italic', 'bullist', 'numlist', 'subscript','superscript', 'link',  'undo', 'redo', 'fullscreen');

    $toolbars['Endnotes'] = array();
	$toolbars['Endnotes'][1] = array('bold', 'underline', 'italic', 'link', 'subscript','superscript', 'numlist', 'undo', 'redo');

	$toolbars['Highlight'] = array();
	$toolbars['Highlight'][1] = array('bold', 'subscript','superscript', 'undo', 'redo');

	$toolbars['Link'] = array();
	$toolbars['Link'][1] = array('link', 'subscript','superscript', 'undo', 'redo');

	$toolbars['Basic Formatting w Link'] = array();
	$toolbars['Basic Formatting w Link'][1] = array('bold', 'underline', 'italic', 'link', 'subscript','superscript', 'undo', 'redo');

	return $toolbars;
}

add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );