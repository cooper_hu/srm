<?php 
// Add relationship Post Object fields to search results
add_filter( 'relevanssi_content_to_index', 'rlv_relationship_content', 10, 2 );
function rlv_relationship_content( $content, $post ) {
	// Authors Relationship Field.
	$relationships = get_post_meta( $post->ID, 'authors', true );
	if ( ! is_array( $relationships ) ) {
		$relationships = array( $relationships );
	}
	foreach ( $relationships as $related_post ) {
		$content .= ' ' . get_the_title( $related_post );
	}

    // Reviewers Relationship Field.
	$relationships = get_post_meta( $post->ID, 'reviewers', true );
	if ( ! is_array( $relationships ) ) {
		$relationships = array( $relationships );
	}
	foreach ( $relationships as $related_post ) {
		$content .= ' ' . get_the_title( $related_post );
	}

    // Featured People Relationship Field.
	$relationships = get_post_meta( $post->ID, 'featured_people', true );
	if ( ! is_array( $relationships ) ) {
		$relationships = array( $relationships );
	}
	foreach ( $relationships as $related_post ) {
		$content .= ' ' . get_the_title( $related_post );
	}
 
	return $content;
}
