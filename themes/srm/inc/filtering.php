<?php 
/* ************************************
 * Category Archive: Load More Articles
 * ************************************ */
function load_more_posts_ajax_handler() {
    // Shows language fallbacks
    if ( class_exists( 'WPML_Display_As_Translated_Tax_Query' ) ) {
        global $sitepress, $wpml_term_translations;
        $wpml_display_as_translated_tax = new WPML_Display_As_Translated_Tax_Query( $sitepress, $wpml_term_translations );
        $wpml_display_as_translated_tax->add_hooks();
    }

    $query_args = array(
        'post_type' => $_POST['post_type'],
        'posts_per_page' => 12,
        'post_status' => 'publish',
        'post__not_in' => $_POST['excluded_ids'],
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'term_id',
                'terms'    => $_POST['cat_id'],
            ),
        ),
        'fields' => 'ids', 
        'paged' => $_POST['paged'],
    );

    $wp_query = new WP_Query($query_args);

    if ($wp_query->have_posts()) {
        while ($wp_query->have_posts()) {
            $wp_query->the_post(); 
            include get_template_directory() . '/template-parts/archive-card-sm.php';
        }
    }

    wp_reset_postdata();

    die();
}

add_action('wp_ajax_load_more_posts', 'load_more_posts_ajax_handler');
add_action('wp_ajax_nopriv_load_more_posts', 'load_more_posts_ajax_handler');

/* ************************************
 * Search: Filter by Post Type
 * ************************************ */
function filter_search_results_ajax_handler() {
    // Shows language fallbacks
    if ( class_exists( 'WPML_Display_As_Translated_Tax_Query' ) ) {
        global $sitepress, $wpml_term_translations;
        $wpml_display_as_translated_tax = new WPML_Display_As_Translated_Tax_Query( $sitepress, $wpml_term_translations );
        $wpml_display_as_translated_tax->add_hooks();
    }

    $query_args = array(
        // 'post_type' => $_POST['post_type'],
        'posts_per_page' => get_option( 'posts_per_page' ),
        'post_status' => 'publish',
        'suppress_filters' => false,
        's' => $_POST['search_term'],
        'relevanssi'  => true,
    );

    if ($_POST['post_type'] === 'all') {
        $query_args['post_type'] = array( 'perspective', 'article', 'video', 'podcast', 'infographic' );
    } else {
        $query_args['post_type'] = $_POST['post_type'];
    }

    $wp_query = new WP_Query($query_args);

    if ($wp_query->have_posts()) {
        while ($wp_query->have_posts()) {
            $wp_query->the_post(); 
            global $post; 
            include get_template_directory() . '/template-parts/archive-card.php';
        }
    }

    wp_reset_postdata();

    die();
}

add_action('wp_ajax_filter_search_results', 'filter_search_results_ajax_handler');
add_action('wp_ajax_nopriv_filter_search_results', 'filter_search_results_ajax_handler');


/* ************************************
 * Search: Load More
 * ************************************ */
function load_more_results_ajax_handler() {
    // Shows language fallbacks
    if ( class_exists( 'WPML_Display_As_Translated_Tax_Query' ) ) {
        global $sitepress, $wpml_term_translations;
        $wpml_display_as_translated_tax = new WPML_Display_As_Translated_Tax_Query( $sitepress, $wpml_term_translations );
        $wpml_display_as_translated_tax->add_hooks();
    }

    $query_args = array(
        // 'post_type' => $_POST['post_type'],
        'posts_per_page' => get_option( 'posts_per_page' ),
        'post_status' => 'publish',
        'suppress_filters' => false,
        // 's' => $_POST['search_term'],
        // 'relevanssi'  => true,
        'paged' => $_POST['page']
    );

    
    if ($_POST['post_type'] === 'all') {
        $query_args['post_type'] = array( 'perspective', 'article', 'video', 'podcast', 'infographic' );
    } else {
        $query_args['post_type'] = $_POST['post_type'];
    }

    if (isset($_POST['search_term'])) {
        $query_args['s'] = $_POST['search_term'];
        $query_args['relevanssi'] = true;
    }

    if (isset($_POST['sort_by'])) {
        if ($_POST['sort_by'] === 'oldest') { 
            $query_args['orderby'] = 'date';
            $query_args['order'] = 'ASC';     
    
        } else if ($_POST['sort_by'] === 'newest') { 
            $query_args['orderby'] = 'date';
            $query_args['order'] = 'DESC';     
    
        } else if ($_POST['sort_by'] === 'ASC') { 
            $query_args['orderby'] = 'title';
            $query_args['order'] = 'ASC';     
    
        } else if ($_POST['sort_by'] === 'DESC') { 
            $query_args['orderby'] = 'title';
            $query_args['order'] = 'DESC';     
    
        }
    }
     
    $wp_query = new WP_Query($query_args);

    if ($wp_query->have_posts()) {
        while ($wp_query->have_posts()) {
            $wp_query->the_post(); 
            global $post; 
            // var_dump($post);
            include get_template_directory() . '/template-parts/archive-card.php';
        }
    }

    wp_reset_postdata();

    die();
}

add_action('wp_ajax_load_more_results', 'load_more_results_ajax_handler');
add_action('wp_ajax_nopriv_load_more_results', 'load_more_results_ajax_handler');


/* ************************************
 * Search: Sort Posts
 * ************************************ */
function sort_post_results_ajax_handler() {
    // Shows language fallbacks
    if ( class_exists( 'WPML_Display_As_Translated_Tax_Query' ) ) {
        global $sitepress, $wpml_term_translations;
        $wpml_display_as_translated_tax = new WPML_Display_As_Translated_Tax_Query( $sitepress, $wpml_term_translations );
        $wpml_display_as_translated_tax->add_hooks();
    }

    $posts_per_page = get_option( 'posts_per_page' ) * ($_POST['page'] - 1);

    $query_args = array(
        'posts_per_page' => $posts_per_page,
        'suppress_filters' => false,
        'post_status' => 'publish',
        // 'paged' => $_POST['page'] - 1
    );

    if ($_POST['post_type'] === 'all') {
        $query_args['post_type'] = array( 'perspective', 'article', 'video', 'podcast', 'infographic' );
    } else {
        $query_args['post_type'] = $_POST['post_type'];
    }

    if (isset($_POST['search_term'])) {
        $query_args['s'] = $_POST['search_term'];
        $query_args['relevanssi'] = true;
    }

    if (isset($_POST['sort_by'])) {
        if ($_POST['sort_by'] === 'oldest') { 
            $query_args['orderby'] = 'date';
            $query_args['order'] = 'ASC';     
    
        } else if ($_POST['sort_by'] === 'newest') { 
            $query_args['orderby'] = 'date';
            $query_args['order'] = 'DESC';     
    
        } else if ($_POST['sort_by'] === 'ASC') { 
            $query_args['orderby'] = 'title';
            $query_args['order'] = 'ASC';     
    
        } else if ($_POST['sort_by'] === 'DESC') { 
            $query_args['orderby'] = 'title';
            $query_args['order'] = 'DESC';     
    
        }
    }
     
    $wp_query = new WP_Query($query_args);

    if ($wp_query->have_posts()) {
        while ($wp_query->have_posts()) {
            $wp_query->the_post(); 
            global $post; 
            // var_dump($post);
            include get_template_directory() . '/template-parts/archive-card.php';
        }
    }

    wp_reset_postdata();

    die();
}

add_action('wp_ajax_sort_post_results', 'sort_post_results_ajax_handler');
add_action('wp_ajax_nopriv_sort_post_results', 'sort_post_results_ajax_handler');


/* ************************************
 * FAQ
 * ************************************ */
function filter_faq_ajax_handler() {
    // Shows language fallbacks
    if ( class_exists( 'WPML_Display_As_Translated_Tax_Query' ) ) {
        global $sitepress, $wpml_term_translations;
        $wpml_display_as_translated_tax = new WPML_Display_As_Translated_Tax_Query( $sitepress, $wpml_term_translations );
        $wpml_display_as_translated_tax->add_hooks();
    }

    $query_args = array(
        'post_type' => 'faq',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'faq-category',
                'field'    => 'term_id',
                'terms'    => $_POST['sort_by'],
            ),
        ),
        'fields' => 'ids', 
    );

    $wp_query = new WP_Query($query_args);

    if ($wp_query->have_posts()) {
        while ($wp_query->have_posts()) {
            $wp_query->the_post(); 
            include get_template_directory() . '/template-parts/faq-item.php';
        }
    }

    wp_reset_postdata();

    die();
}

add_action('wp_ajax_filter_faq', 'filter_faq_ajax_handler');
add_action('wp_ajax_nopriv_filter_faq', 'filter_faq_ajax_handler');