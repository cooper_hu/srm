<?php 

// Remove time limit on forms (Formidable)
// add_filter( 'frm_time_to_check_duplicates', '__return_false' );

// Airtable Integration
add_action('frm_after_create_entry', 'ff_to_airtable', 30, 2);

function ff_to_airtable($entry_id, $form_id) {
    if ($form_id === 2 || $form_id === 6 || $form_id === 7) {// Airtable API endpoint and credentials
        $apiKey = 'patLXdPXxn0SDTBw5.cccc7004bb81d83e2458c2859693cbef973177dceafa549b81b9b5c8eb595b88';
        $baseId = 'appyQ2jkzM0i87D0B';
        $tableName = 'tblyItvWtkmVX0Mi4';

        if ($form_id === 7) {
            $language = 'French';
            $name = $_POST['item_meta'][26];
            $email = $_POST['item_meta'][27];
            $question = $_POST['item_meta'][28];
        } elseif ($form_id === 6) {
            $language = 'Spanish';
            $name = $_POST['item_meta'][22];
            $email = $_POST['item_meta'][23];
            $question = $_POST['item_meta'][24];
        } elseif ($form_id === 2) {
            $language = 'English';
            $name = $_POST['item_meta'][7];
            $email = $_POST['item_meta'][8];
            $question = $_POST['item_meta'][9];
        }

        // Airtable API URL
        $url = "https://api.airtable.com/v0/{$baseId}/{$tableName}";

        // Data to insert into Airtable
        $data = [
            'records' => [
                [
                    'fields' => [
                        'Name' => $name,
                        'Email' => $email,
                        'Question' => $question,
                        'Language' => $language,
                    ]
                ]
            ]
        ];

        // Convert data to JSON
        $jsonData = json_encode($data);

        // Initialize cURL
        $ch = curl_init($url);

        // Set cURL options
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer {$apiKey}",
            'Content-Type: application/json'
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);

        // Execute cURL and get the response
        $response = curl_exec($ch);

        // Check for errors
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        } else {
            echo 'Response:' . $response;
        }

        // Close cURL
        curl_close($ch);
    }
}
