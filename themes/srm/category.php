<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SRM
 */

get_header(); 
$category = get_queried_object(); ?>

<main id="primary" class="site-main">
    <?php // Search ?>
    <section class="section xs-margin">
        <div class="container">
            <div class="">
                <?php get_template_part( 'template-parts/search-bar'); ?>
            </div>
        </div>
    </section>

    <?php // Header ?>
    <section class="section">
		<div class="container">
			<div class="container__inner">
                <header class="category-header">
                    <h3 class=""><?php printf(esc_html__( 'Topic', 'srm')); ?></h3>
                    
                    <h1 class="category-title"><?php single_cat_title(); ?></h1>

                    <?php get_template_part( 'template-parts/category-subnav'); ?>
                    
                    <?php if ( category_description() ) : ?>
                        <div class="category-description">
                            <?php echo category_description(); ?>
                        </div>
                    <?php endif; ?>
                </header>
			</div>
		</div>
	</section>

    <?php // Featured Content
    $featured_posts = get_field('featured_content');
    $featured_posts_headline = 'Featured Content';  
    
    // Contenido destacado

    ?>
    <?php if ( $featured_posts ) : 
        $count = 1; ?>

        <section class="section">
            <div class="container">
                <div class="container__inner-full">
                    <h2><?php printf(esc_html__( 'Featured Content', 'srm')); ?></h2>
                    <div class="archive-grid">
                        <?php foreach( $featured_posts as $post ): setup_postdata($post); ?>
                            <?php if ($count === 1) : ?>
                                <?php get_template_part( 'template-parts/archive-card-wide'); ?>
                            <?php else : ?>
                                <?php get_template_part( 'template-parts/archive-card'); ?>
                            <?php endif; ?>
                            <?php $count++; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </section>
        <?php wp_reset_postdata(); ?>
    <?php endif; ?>

    <?php // Content Groups 
    if (have_rows('content_groups', $category)) :
        while (have_rows('content_groups', $category)) : the_row(); 
            $section_headline = get_sub_field('section_headline');
            $section_items = get_sub_field('section_items'); ?>
            <section class="section">
                <div class="container">
                    <div class="container__inner-full">
                        <h2><?= $section_headline; ?></h2>
                        <?php if ( $section_items ) : ?>
                            <div class="archive-grid">
                                <?php foreach( $section_items as $post ): setup_postdata($post); ?>
                                    <?php get_template_part( 'template-parts/archive-card'); ?>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </section>
        <?php endwhile;
    endif; ?>
 


    <?php // Get the current category object

    // Get all custom post types
    $args = array(
        'public'   => true, // Get only public post types
        '_builtin' => false, // Exclude WordPress default post types like 'post' and 'page'
    );

    $section_count = 1;
    $posts_per_page = get_option( 'posts_per_page' );
    $post_types = get_post_types($args, 'objects');
    
    // Display the post count for each post type in this category
    foreach ($post_types as $post_type) :
        $post_name = $post_type->name;
        $cat_id = $category->term_id;

        // Arguments to count posts in this category and of this custom post type
        $query_args = array(
            'post_type' => $post_name,
            'posts_per_page' => $posts_per_page,
            'orderby'   => array(
                'date' =>'DESC',
            ),
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'term_id',
                    'terms'    => $cat_id,
                ),
            ),
            'fields' => 'ids', 
        );
        
        // Execute the query
        $the_query = new WP_Query($query_args); 
        if ($the_query->have_posts()) : 
            $total_pages = $the_query->max_num_pages; 
            $total_posts = $the_query->found_posts;

            $excluded_posts = array(); ?>
            <section class="section no-margin padded-lg <?= ($section_count % 2 == 0) ? '' : 'off-white'; ?>" id="<?= $post_type->name; ?>">
                <div class="container">
                    <div class="container__inner-full">
                        <h2 class="css-capitalize">
                            <?php if (get_field('shortname', $category)) : ?>
                                <?= get_field('shortname', $category); ?>
                            <?php endif; ?>
                            <?= $post_type->labels->name; ?>
                        </h2>
                        <div class="category-wrapper">
                            <div class="category-wrapper__inner">
                                <div class="archive-grid">
                                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                        <?php 
                                        $hide_label = true; // hides meta post type label in top of card
                                        include get_template_directory() . '/template-parts/archive-card.php';
                                        array_push($excluded_posts, get_the_ID()); ?>
                                    <?php endwhile; ?>
                                </div>

                                <div class="archive-grid-sm <?= $post_name; ?>"></div>

                                <?php if ($posts_per_page < $total_posts) : ?>
                                    <div class="archive-grid-footer">
                                        <button class="btn blue-gradient wide js-load-more-cat" 
                                            data-paged="1" 
                                            data-init-post-count="<?= $posts_per_page; ?>"
                                            data-post-type="<?= $post_name; ?>" 
                                            data-category-id="<?= $cat_id; ?>" 
                                            data-excluded-ids="<?= json_encode($excluded_posts); ?>" 
                                            data-total-posts="<?= $total_posts; ?>">
                                            Load More
                                        </button>
                                        <div class="loading-spinner" style="display: none;">Loading...</div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php 
        $section_count++;
        endif; ?>
       <?php wp_reset_postdata();
      
    endforeach; ?>
</main>

<?php get_footer();