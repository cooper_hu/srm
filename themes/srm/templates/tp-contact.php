<?php
/**
 * Template Name: Contact
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package around
 */

get_header();


$form_shortcode = get_field('form_shortcode');
$headline = get_field('headline');
$description = get_field('description'); ?>

<main id="primary" class="site-main">

    <section class="section xs-margin">
        <div class="container">
            <div class="">
                <?php get_template_part( 'template-parts/search-bar'); ?>
            </div>
        </div>
    </section>
            
    <section class="section">
        <div class="container">
            <div class="container__inner">
                <div class="text-wrapper faq-section-header">
                    <?php 

                    if ($headline) : ?>
                        <h1><?= $headline; ?></h1>
                    <?php endif; ?>

                    <?php if ($description) : ?>
                        <?= $description; ?>
                    <?php endif; ?>
                </div>
                
                <?php if ($form_shortcode) : ?>
                    <div class="faq-form">
                        <?= do_shortcode($form_shortcode); ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </section>

</main><!-- #main -->

<?php get_footer();
