<?php
/**
 * Template Name: FAQ
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package around
 */

get_header();


$form_shortcode = get_field('form_shortcode');
$featured_questions = get_field('featured_questions'); ?>

<main id="primary" class="site-main">

    <section class="section xs-margin">
        <div class="container">
            <div class="">
                <?php get_template_part( 'template-parts/search-bar'); ?>
            </div>
        </div>
    </section>
            
    <section class="section">
        <div class="container">
            <div class="container__inner">

                <div class="text-wrapper faq-section-header">
                    <?php $headline = get_field('headline');
                    $description = get_field('description');
                    
                    if ($headline) : ?>
                        <h1><?= $headline; ?></h1>
                    <?php endif; ?>

                    <?php if ($description) : ?>
                        <?= $description; ?>
                    <?php endif; ?>
                    
                </div>
                
                <?php if ( $featured_questions ) : ?>
                    <div class="accordion-group">
                        <div class="faq-header">
                            <div class="faq-header__left">
                                
                                <?php $terms = get_terms(array(
                                    'taxonomy' => 'faq-category',
                                    'hide_empty' => true,
                                )); ?>

                                <?php if (!empty($terms) && !is_wp_error($terms)) : ?>
                                    <div class="sort-filter-wrapper">
                                        <span><?php esc_html_e( 'Categories', 'srm' ); ?>:</span>
                                        <select class="sort-filter js-filter-faq">
                                            <option value="featured"><?php esc_html_e( 'Featured FAQs', 'srm' ); ?></option>
                                            <?php foreach ($terms as $term) : ?>
                                                <option value="<?= esc_html($term->term_id); ?>"><?= esc_html($term->name); ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        <div class="js-featured-faqs">
                            <?php foreach( $featured_questions as $post ): setup_postdata($post); ?>
                                <?php include get_template_directory() . '/template-parts/faq-item.php'; ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="js-filtered-faqs">

                        </div>
                    </div>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>

                <?php if ($form_shortcode) : ?>
                    <div class="faq-form">
                        <h2><?php printf(esc_html__('Got a question about SRM? Ask us below!', 'srm')); ?></h2>
                        <?= do_shortcode($form_shortcode); ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </section>

</main><!-- #main -->

<?php get_footer();
