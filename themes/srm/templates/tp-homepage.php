<?php
/**
 * Template Name: Homepage
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package around
 */

get_header();

$featured_posts = get_field('featured_posts');
$featured_posts_headline = get_field('featured_posts_headline'); ?>

<main id="primary" class="site-main">
    <section class="section no-top-margin xs-bottom-margin">
        <div class="container">
            <div class="">
                <div class="homepage-header">
                    <div class="homepage-header__inner">
                        <?php $logo = get_field('logo');
                        if ($logo) : ?>
                            <img class="homepage-header-logo" src="<?= $logo['url']; ?>" alt="SRM Logo"/>
                        <?php endif; ?>

                        <?php if (get_field('intro')) : ?>
                            <?= get_field('intro'); ?>
                        <?php endif; ?>
						
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="">
        <div class="container">
            <div class="">
                <?php get_template_part( 'template-parts/search-bar'); ?>
            </div>
        </div>
    </section>
                
    <section class="section">
        <div class="container">
            <div class="container__inner-full">
                <h2><?= $featured_posts_headline; ?></h2>
                <?php if ( $featured_posts ) : 
                    $count = 1; ?>
                    <div class="archive-grid four-across">
                        <?php foreach( $featured_posts as $post ): setup_postdata($post); ?>
                            <?php if ($count === 1) : ?>
                                <?php get_template_part( 'template-parts/archive-card-wide'); ?>
                            <?php else : ?>
                                <?php get_template_part( 'template-parts/archive-card'); ?>
                            <?php endif; ?>
                            <?php $count++; ?>
                        <?php endforeach; ?>
                    </div>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <?php
    $explorer_posts = get_field('explorer_posts');
    if( $explorer_posts ): ?>

        <section class="section">
            <div class="container">
                <div class="container__inner-full">
                    <div class="homepage-explorer">
                        <?php $explorer_headline = get_field('explorer_posts_headline'); 
                        if ($explorer_headline) : ?>
                            <h2><?= $explorer_headline; ?></h2>
                        <?php endif; ?>
                        <div class="explorer-grid">
                            <?php foreach( $explorer_posts as $post ) : setup_postdata($post); ?>
                                <?php get_template_part( 'template-parts/explorer-card'); ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php wp_reset_postdata(); ?>
    <?php endif; ?>
</main><!-- #main -->

<?php get_footer();
