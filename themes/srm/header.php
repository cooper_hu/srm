<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SRM
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<link rel="apple-touch-icon" sizes="180x180" href="<?= get_template_directory_uri(); ?>/apple-touch-icon.png">
	<link rel="icon" type="image/x-icon" href="<?= get_template_directory_uri(); ?>/favicon.ico">

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Titillium+Web:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&display=swap" rel="stylesheet">

	<?php wp_head(); ?>

	<?php if (is_single()) : ?>
		<meta name="citation_title" content="<?= get_the_title(); ?>"/>
		<meta name="citation_fulltext_html_url" content="<?= get_the_permalink(); ?>"/>
    	<meta name="citation_fulltext_world_readable" content=""/>
		<meta name="citation_publication_date" content="<?= get_the_date('Y/m/d'); ?>"/>
    	<meta name="citation_journal_title" content="SRM360"/>
    	<meta name="citation_journal_abbrev" content="SRM360"/>

		<?php // Authors
		$authors = get_field('authors');
        if ($authors) : ?>
			<?php foreach( $authors as $author ) : ?> 
				<meta name="citation_author" content="<?= get_the_title($author); ?>"/>
			<?php endforeach; ?>
        <?php endif; ?>
		
	<?php endif; ?>

	<style>
		html {
			margin-top: 0 !important;
		}
	</style>
	<?php /* Citations 
	<meta name="citation_title" content="Homelessness"/>
    <meta name="citation_fulltext_html_url" content="https://ourworldindata.org/homelessness"/>
    <meta name="citation_fulltext_world_readable" content=""/>
    <meta name="citation_publication_date" content="2024/03/13"/>
    <meta name="citation_journal_title" content="Our World in Data"/>
    <meta name="citation_journal_abbrev" content="Our World in Data"/>
    <meta name="citation_author" content="Bastian Herre"/>
    <meta name="citation_author" content="Pablo Arriagada"/>
    <meta name="citation_author" content="Max Roser"/>
	*/ ?>

	<?php // Twitter conversion tracking base code ?>
	<script>
		!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
		},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='https://static.ads-twitter.com/uwt.js',
		a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
		twq('config','opp8f');
	</script>

</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<header class="site-header">
	<div class="container">
		<div class="container__inner--fluid">
			<?php if (defined('ICL_SITEPRESS_VERSION')) {
				// WPML is active
				$current_language = apply_filters('wpml_current_language', null);

				if ($current_language === 'en') {
					$homeUrl = '/';
				} elseif ($current_language === 'es') {
					$homeUrl = '/es';
				} elseif ($current_language === 'fr') {
					$homeUrl = '/fr';
				}
			} ?>

			<a class="site-header-logo" href="<?= $homeUrl; ?>">
				<img src="<?= get_template_directory_uri(); ?>/assets/images/srm-logo.png" alt="SRM360 Logo"/>
			</a>

			<?php // = do_shortcode('[wpml_language_switcher]'); ?>
			<a href="#mobile-nav" class="mobile-nav-button js-open-mobile-nav">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</a>

			<div class="nav-button-group">
				<div class="site-header-language">
					<?php $lgargs = array('type' => 'widget', 'native' => 1, 'translated' => 0 );
					do_action( 'wpml_language_switcher', $lgargs); ?>
				</div>

				<a href="#main-nav" class="nav-button secondary js-open-secondary-nav">
					<?php printf(esc_html__('Topics', 'srm')); ?>
					<span class="nav-button__hamburger"></span>
				</a>

				<a href="#secondary-nav" class="nav-button js-open-nav">
					<?php printf(esc_html__('About SRM360', 'srm')); ?>
					<span class="nav-button__hamburger"></span>
				</a>
			</div>

			<?php // About SRM360 Menu ?>
			
			
		</div>
	</div>
</header>

<div id="main-nav" class="mfp-hide mobile-nav">
	<nav id="site-navigation" class="main-navigation">
		<div class="nav-close-button js-close-nav"></div>
		<div class="main-navigation__left">
			
		<?php wp_nav_menu(
				array(
					'theme_location' => 'content-menu',
				)
			); ?>
			
		</div>
		<div class="main-navigation__right">
			<?php wp_nav_menu(
				array(
					'theme_location' => 'main-menu-right',
					'menu_id'        => 'primary-menu-right',
				)
			); ?>
		</div>
	</nav>
</div>

<div id="secondary-nav" class="mfp-hide mobile-nav-secondary">
	<nav id="secondary-navigation" class="secondary-navigation">
		<div class="nav-close-button js-close-nav"></div>
		<div class="secondary-navigation__left">
			<?php wp_nav_menu(
				array(
					'theme_location' => 'main-menu-left',
					'menu_id'        => 'primary-menu-left',
				)
			); ?>
		</div>
	</nav>
</div>

<div id="mobile-nav" class="mfp-hide mobile-nav">
	<nav id="mobile-navigation" class="mobile-navigation">
		<div class="nav-close-button js-close-nav"></div>
		
		<div>
			<?php wp_nav_menu(
				array(
					'theme_location' => 'content-menu',
				)
			); ?>
		</div>

		<div>
			<?php wp_nav_menu(
				array(
					'theme_location' => 'main-menu-left',
					'menu_id'        => 'primary-menu-left',
				)
			); ?>
		</div>
		<div>
			<?php wp_nav_menu(
				array(
					'theme_location' => 'main-menu-right',
					'menu_id'        => 'primary-menu-right',
				)
			); ?>
		</div>
	</nav>
</div>
